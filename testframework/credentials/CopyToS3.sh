#!/bin/bash

# make/edit the file called ${filename} first with the structure shown in the file

# filename=$1
# encryptedfilename=$2


aws kms encrypt --key-id arn:aws:kms:eu-west-2:109964479621:key/96c28ee1-cfbc-4dec-aada-c6477dd09f1c --region eu-west-2 --plaintext fileb://${filename} --output text --query CiphertextBlob  | base64 --decode > "${encryptedfilename}"

# aws kms encrypt --key-id arn:aws:kms:eu-west-2:109964479621:key/96c28ee1-cfbc-4dec-aada-c6477dd09f1c --region eu-west-2 --plaintext fileb://AwsUserCred --output text --query CiphertextBlob  | base64 --decode > Encrypted_AwsUserCred

rm ${filename}
# rm AwsUserCred

aws s3 cp ${encryptedfilename} s3://gradstestbucket
# aws s3 cp Encrypted_AwsUserCred s3://gradstestbucket

# rm Encrypted_AwsUserCred
rm ${encryptedfilename}
