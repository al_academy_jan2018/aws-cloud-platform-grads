#!/bin/bash

# bucketfile=$1
# decryptfilename=$2

aws s3 cp s3://gradstestbucket/${bucketfile} .
# aws s3 cp s3://gradstestbucket/Encrypted_AwsUserCred .

aws kms decrypt --region eu-west-2 --ciphertext-blob fileb://${bucketfile} --output text --query Plaintext | base64 --decode > "${decryptfilename}"

# aws kms decrypt --region eu-west-2 --ciphertext-blob fileb://Encrypted_AwsUserCred --output text --query Plaintext | base64 --decode > AwsUserCred

# rm Encrypted_AwsUserCred
rm "${bucketfile}"
