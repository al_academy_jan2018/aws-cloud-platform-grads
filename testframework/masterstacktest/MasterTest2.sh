#!/bin/bash

# First enter the region, then the message mode (see next comment), then the product(s) that you want to test if they are up at AWS Service Catalog. Type ALL instead of product name to test all products in the TestAll.sh and Test2All.sh script including the portfolio(s), add more manually using the same format in TestAll.sh and Test2All.sh to test new products and portfolios.

# messagemode of 0 would result in all messages being sent to slack, any other input of messagemode would display missing products only (i.e. test failed),

# AwsLoginCred can be changed through the documents in gradstestbucket called AwsLoginCred.sh, if you wish to switch user to access the service catalog for master stack endpoint testing.

# region=$1
# shift
# messagemode=$1
# shift

# jenkins makes them env variable
# export region
# export messagemode

# jenkins gives the product_list as well
IFS=' ' read -r -a products <<< "$product_list"

if [[ "ALL" = $product_list ]]
then
  ./RunTestAll2.sh
  echo "Test Completed."
  exit 0
fi

for product in $product_list
do
  product=$product ./RunSmallTest2.sh
done

echo "Test Completed"
