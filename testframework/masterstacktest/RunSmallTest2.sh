#!/bin/bash

if [[ -z ${region+x} ]]
then
  if [[ 2 != $# ]]
  then
    echo "Error: Please input the region first and one product for testing."
    exit 1
  fi
  region=$1
  product=$2
fi

# checks if assert and phantomjs installed on local machine in this repo
if ! ls -d assert.sh >/dev/null 2>&1
then
  wget https://raw.githubusercontent.com/lehmannro/assert.sh/master/assert.sh
fi

if ! uname | grep Linux >/dev/null 2>&1
then
  # Install Phantom for Mac
  if ! ls -d phantomjs-*mac* >/dev/null 2>&1
  then
    wget https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-macosx.zip
    unzip phantomjs-2.1.1-macosx.zip
    rm phantomjs-2.1.1-macosx.zip
  fi

  phantomjs="phantomjs-2.1.1-macosx/bin/phantomjs"
else
  if ! ls -d phantomjs-*linux* >/dev/null 2>&1
  then
    # install the assert and phantomjs for jenkins' use
    wget https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-linux-x86_64.tar.bz2
    tar -xvf phantomjs-2.1.1-linux-x86_64.tar.bz2
    rm phantomjs-2.1.1-linux-x86_64.tar.bz2
  fi
  phantomjs="phantomjs-2.1.1-linux-x86_64/bin/phantomjs"
fi


aws s3 cp s3://gradstestbucket/Encrypted_AwsLoginCred . >/dev/null 2>&1
aws kms decrypt --region eu-west-2 --ciphertext-blob fileb://Encrypted_AwsLoginCred --output text --query Plaintext | base64 --decode > AwsLoginCred.sh

accountalias=$(grep accountalias AwsLoginCred.sh | cut -d '"' -f 2)
usrname=$(grep username AwsLoginCred.sh | cut -d '"' -f 2)
psword=$(grep password AwsLoginCred.sh | cut -d '"' -f 2)

rm Encrypted_AwsLoginCred
rm AwsLoginCred.sh

export accountalias
export usrname
export psword
export region
export product
export phantomjs

testoutput=$(./SmallTestProd.sh)

result1=$(echo "$testoutput" | grep "tests passed")
result3=$(echo "$testoutput" | grep "tests failed")

if [[ -z $result1 ]]
then
   mainmessage1="Error - ${product} is missing in the Service Catalogue."
   mainmessage2="$result3"
   mainmessage3=""

   export mainmessage1
   export mainmessage2
   export mainmessage3
   ./cfnote.py

elif (( messagemode==0 ))
then
   mainmessage1="${product} is present in the Service Catalogue."
   mainmessage2="$result1"
   mainmessage3=""
   export mainmessage1
   export mainmessage2
   export mainmessage3
   ./cfnote.py
fi

rm AwsSCProducts.html
