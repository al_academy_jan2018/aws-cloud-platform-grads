#!/bin/bash

# region=$1
# export region

# checks if assert and phantomjs installed on local machine in this repo
if ! ls -d assert.sh >/dev/null 2>&1
then
  wget https://raw.githubusercontent.com/lehmannro/assert.sh/master/assert.sh
fi

if ! uname | grep Linux >/dev/null 2>&1
then
  # Install Phantom for Mac
  if ! ls -d phantomjs-*mac* >/dev/null 2>&1
  then
    wget https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-macosx.zip
    unzip phantomjs-2.1.1-macosx.zip
    rm phantomjs-2.1.1-macosx.zip
  fi

  phantomjs="phantomjs-2.1.1-macosx/bin/phantomjs"
else

  if ! ls -d phantomjs-*linux* >/dev/null 2>&1
  then
    # install the assert and phantomjs for jenkins' use
    wget https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-linux-x86_64.tar.bz2
    tar -xvf phantomjs-2.1.1-linux-x86_64.tar.bz2
    rm phantomjs-2.1.1-linux-x86_64.tar.bz2
  fi
  phantomjs="phantomjs-2.1.1-linux-x86_64/bin/phantomjs"
fi


aws s3 cp s3://gradstestbucket/Encrypted_AwsLoginCred . >/dev/null 2>&1
aws kms decrypt --region eu-west-2 --ciphertext-blob fileb://Encrypted_AwsLoginCred --output text --query Plaintext | base64 --decode > AwsLoginCred.sh

accountalias=$(grep accountalias AwsLoginCred.sh | cut -d '"' -f 2)
usrname=$(grep username AwsLoginCred.sh | cut -d '"' -f 2)
psword=$(grep password AwsLoginCred.sh | cut -d '"' -f 2)

rm Encrypted_AwsLoginCred
rm AwsLoginCred.sh

export accountalias
export usrname
export psword
export phantomjs
export product

echo "Beginning AWS test..."
testoutput=$(./TestAll.sh)
# echo $testoutput

result1=$(echo "$testoutput" | grep "tests passed")
result2=$(echo "$testoutput" | grep "aws servicecatalog" | cut -d ' ' -f 14 | sed 's/"//g')
result3=$(echo "$testoutput" | grep "tests failed")

if [[ -z $result1 ]]
then
   mainmessage1="Error - The following failed the AWS test and are missing in the Service Catalogue: "
   mainmessage2="$result2"
   mainmessage3="$result3 PhantomJs endpoint test not ran as a result."
   export mainmessage1
   export mainmessage2
   export mainmessage3
   ./cfnote.py
   exit 1
elif (( messagemode==0 ))
then
   mainmessage1="AWS test finished running."
   mainmessage2="$result1"
   mainmessage3="Preparing for PhantomJs test..."
   export mainmessage1
   export mainmessage2
   export mainmessage3
   ./cfnote.py
fi

echo "AWS test passed."
echo "Beginning PhantomJs endpoint test..."

testoutput2=$(./Test2All.sh)

result21=$(echo "$testoutput2" | grep "tests passed")
result22=$(echo "$testoutput2" | grep "AwsSC" | cut -d ' ' -f 4 | sed 's/"//g')
result23=$(echo "$testoutput2" | grep "tests failed")

if [[ -z $result23 ]]
then
    mainmessage1="Master stack endpoint testing passed!"
    mainmessage2="$result21"
    mainmessage3="All portfolios and products are present in the Service Catalogue."
else
   mainmessage1="Error - The following are missing in the Service Catalogue: "
   mainmessage2="$result22"
   mainmessage3="$result23 All other products/portfolios have passed the test."
fi
# need a more meaningful message in the future
export mainmessage1
export mainmessage2
export mainmessage3

./cfnote.py

rm AwsSCPortfolios.html
rm AwsSCProducts.html
