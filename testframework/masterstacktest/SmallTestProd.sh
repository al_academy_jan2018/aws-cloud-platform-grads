#!/bin/bash

source ./assert.sh

assert_raises "aws servicecatalog search-products --region ${region} | jq -r --arg product "$product" '.ProductViewSummaries[] | select(.Name==\"$product\") | .Name' | grep ${product}"

${phantomjs} aws2.js >/dev/null

assert_raises "grep ${product} AwsSCProducts.html"

assert_end







# if aws servicecatalog search-products --region ${region} | jq -r --arg product "$product" '.ProductViewSummaries[] | select(.Name==$product) | .Name' | grep ${product}
# then
#   echo "Works ok in shell"
# else
#   echo "Failed"
#   exit 1
# fi



# disProduct="XXXXXXXXXX"
# # Check for discontinued product
# assert_raises "aws servicecatalog search-products --region us-east-1 | jq -r '.ProductViewSummaries[].Name' | grep ${disProduct}" 1
