#!/bin/bash

# imports the  assert library (read in tht file and make whatever in that file local to me)
source ./assert.sh

# Check that the ALCloud portfolio is there
assert_raises "aws servicecatalog list-portfolios --region ${region} | jq -r '.PortfolioDetails[].DisplayName' | grep ALCloud"

# # Catalogue contains 9 entries
# assert "aws servicecatalog search-products --region ${region} | jq -r '.ProductViewSummaries[].Name' | wc -l | sed -e 's/ //g'" "9"

# Check for specific product
assert_raises "aws servicecatalog search-products --region ${region} | jq -r '.ProductViewSummaries[].Name' | grep CentralLogging" 
assert_raises "aws servicecatalog search-products --region ${region} | jq -r '.ProductViewSummaries[].Name' | grep CloudPlatform-Demo"
assert_raises "aws servicecatalog search-products --region ${region} | jq -r '.ProductViewSummaries[].Name' | grep Django-CloudPlatform"
assert_raises "aws servicecatalog search-products --region ${region} | jq -r '.ProductViewSummaries[].Name' | grep Flask-CloudPlatform"
assert_raises "aws servicecatalog search-products --region ${region} | jq -r '.ProductViewSummaries[].Name' | grep Java-CloudPlatform"
assert_raises "aws servicecatalog search-products --region ${region} | jq -r '.ProductViewSummaries[].Name' | grep PHP-CloudPlatform"
assert_raises "aws servicecatalog search-products --region ${region} | jq -r '.ProductViewSummaries[].Name' | grep Rails-CloudPlatform"
assert_raises "aws servicecatalog search-products --region ${region} | jq -r '.ProductViewSummaries[].Name' | grep Serverless-CloudPlatform"
assert_raises "aws servicecatalog search-products --region ${region} | jq -r '.ProductViewSummaries[].Name' | grep Windows-CloudPlatform"

assert_end
# disProduct="XXXXXXXXXX"
# # Check for discontinued product
# assert_raises "aws servicecatalog search-products --region ${region} | jq -r '.ProductViewSummaries[].Name' | grep ${disProduct}" 1
