#!/usr/bin/python

import json
import requests # pip install requests
import sys
import os

ch1 = "@vincent.cheung"
# ch2 = "#vladzs"
ch3 = "#jan2018_academy"
# ch4 = "#nginx_and_chill"

incomingwebhook = "https://hooks.slack.com/services/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"
#incomingwebhook = "https://hooks.slack.com/services/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"
username = "AWS Service Catalog"
channel = ch3
# mainmsg1 = os.environ["mainmessage1"]
# mainmsg2 = os.environ["mainmessage2"]
title = ""
imageurl = ""
#sys.argv[1]

def slacktest(message):
    payload = {
        "channel": channel,
        "username": username,
        "text": message,
        "icon_emoji": ":aws:",
        "attachments": [
            {
                "title": title,
                "image_url": imageurl
            }
        ]
    }
    req = requests.post(incomingwebhook, json.dumps(payload), headers={'content-type': 'application/json'})

slacktest(os.environ["mainmessage1"])
slacktest(os.environ["mainmessage2"])
# only run if there is a mainmessage3 variable
if os.environ["mainmessage3"] != "":
    slacktest(os.environ["mainmessage3"])
