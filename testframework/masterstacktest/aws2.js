var system = require('system');
var env = system.env;
var steps=[];
var testindex = 0;
var loadInProgress = false;//This is set to true when a page is still loading
var usrname = env['usrname'];
var psword = env['psword'];
var accountalias = env['accountalias'];

/*********SETTINGS*********************/
var webPage = require('webpage');
var page = webPage.create();
page.settings.userAgent = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36';
page.settings.javascriptEnabled = true;
page.settings.loadImages = false;//Script is much faster with this field set to false
phantom.cookiesEnabled = true;
phantom.javascriptEnabled = true;
/*********SETTINGS END*****************/

console.log('All settings loaded, start with execution');
page.onConsoleMessage = function(msg) {
    console.log(msg);
};
/**********DEFINE STEPS THAT FANTOM SHOULD DO***********************/
steps = [
    function(){
        console.log('Step 1 - Open Amazon log in page');
        page.open("https://console.aws.amazon.com/console/home", function(status){

		});
    },

	  function(accountalias){
        console.log('Step 2 - Enter automationlogic user');
		    page.evaluate(function(accountalias){
		        document.getElementById("resolving_input").value= accountalias;
			      document.getElementById("next_button").click();
		}, accountalias);
    },

	  function(accountalias,usrname,psword){
        console.log('Step 3 - Enter username and password');
		    page.evaluate(function(accountalias,usrname,psword){
			       document.getElementById("username").value= usrname;
             document.getElementById("password").value= psword;
             document.getElementById("resync_button").click();
		}, accountalias, usrname, psword);
    },
	  function(){
        console.log('Step 4 - Open AWS Service Catalog Products list page');
        page.open("https://console.aws.amazon.com/servicecatalog/home?region=us-east-1#/products", function(status){

		});
    },

    function(){
		console.log("Step 5 - Content of the page is saved");
         var fs = require('fs');
		     var result = page.evaluate(function() {
			        return document.querySelectorAll("html")[0].outerHTML;
		         });
        fs.write('AwsSCProducts.html',result,'w');
    },
];
/**********END STEPS THAT FANTOM SHOULD DO***********************/

//Execute steps one by one
interval = setInterval(executeRequestsStepByStep,50);

function executeRequestsStepByStep(){
    if (loadInProgress == false && typeof steps[testindex] == "function") {
        //console.log("step " + (testindex + 1));
        steps[testindex](accountalias,usrname,psword);
        testindex++;
    }
    if (typeof steps[testindex] != "function") {
        console.log("test complete!");
        phantom.exit();
    }
}

/**
 * These listeners are very important in order to phantom work properly. Using these listeners, we control loadInProgress marker which controls, weather a page is fully loaded.
 * Without this, we will get content of the page, even a page is not fully loaded.
 */
page.onLoadStarted = function() {
    loadInProgress = true;
    console.log('Loading started');
};
page.onLoadFinished = function() {
    loadInProgress = false;
    console.log('Loading finished');
};
page.onConsoleMessage = function(msg) {
    console.log(msg);
};
