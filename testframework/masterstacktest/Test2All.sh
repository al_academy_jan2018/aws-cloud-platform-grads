#!/bin/bash

# imports the  assert library (read in tht file and make whatever in that file local to me)
source ./assert.sh

${phantomjs} aws.js
${phantomjs} aws2.js

assert_raises "grep ALCloud AwsSCPortfolios.html"
assert_raises "grep CentralLogging AwsSCProducts.html" 
assert_raises "grep CloudPlatform-Demo AwsSCProducts.html"
assert_raises "grep Django-CloudPlatform AwsSCProducts.html"
assert_raises "grep Flask-CloudPlatform AwsSCProducts.html"
assert_raises "grep Java-CloudPlatform AwsSCProducts.html"
assert_raises "grep PHP-CloudPlatform AwsSCProducts.html"
assert_raises "grep Rails-CloudPlatform AwsSCProducts.html"
assert_raises "grep Serverless-CloudPlatform AwsSCProducts.html"
assert_raises "grep Windows-CloudPlatform AwsSCProducts.html"

assert_end
