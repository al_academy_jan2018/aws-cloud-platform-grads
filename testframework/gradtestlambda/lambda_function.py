import boto3


def lambda_handler(event, context):
    client = boto3.client('cloudformation')
    response = client.create_stack(
        StackName='GradsTestEnv',
        TemplateURL='https://s3.eu-west-2.amazonaws.com/gradstestbucket/exampletemplate.yaml',
        TimeoutInMinutes=123,
        Capabilities=[
            'CAPABILITY_NAMED_IAM',
        ],
        OnFailure='ROLLBACK',
        Tags=[
            {
                'Key': 'Name',
                'Value': 'GradsTestEnv'
            },
        ]
    )
