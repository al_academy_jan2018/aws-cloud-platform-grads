{
    "AWSTemplateFormatVersion": "2010-09-09",
    "Description": "API-Gateway-triggered Lambda functions for testing product.",
    "Parameters": {
        "CustomDomainName": {
            "Description": "Use a custom domain name for the webhook endpoint, if left blank API Gateway will create a domain name for you",
            "Type": "String",
            "Default": ""
        },
        "QSS3BucketName": {
            "AllowedPattern": "^[0-9a-zA-Z]+([0-9a-zA-Z-]*[0-9a-zA-Z])*$",
            "ConstraintDescription": "Quick Start bucket name can include numbers, lowercase letters, uppercase letters, and hyphens (-). It cannot start or end with a hyphen (-).",
            "Default": "gradstestbucket",
            "Description": "S3 bucket name for the Quick Start assets. Quick Start bucket name can include numbers, lowercase letters, uppercase letters, and hyphens (-). It cannot start or end with a hyphen (-).",
            "Type": "String"
        },
        "QSS3KeyPrefix": {
            "AllowedPattern": "^[0-9a-zA-Z-/]*$",
            "ConstraintDescription": "Quick Start key prefix can include numbers, lowercase letters, uppercase letters, hyphens (-), and forward slash (/).",
            "Default": "git2s3/latest/",
            "Description": "S3 key prefix for the Quick Start assets. Quick Start key prefix can include numbers, lowercase letters, uppercase letters, hyphens (-), and forward slash (/).",
            "Type": "String"
        }
    },
    "Conditions": {
        "UseCustomDomain": {
            "Fn::Not": [
                {
                    "Fn::Equals": [
                        {
                            "Ref": "CustomDomainName"
                        },
                        ""
                    ]
                }
            ]
        }
    },
    "Resources": {
        "LambdaZipsBucket": {
            "Type": "AWS::S3::Bucket",
            "Properties": {
                "Tags": []
            }
        },
        "CopyZips": {
            "DependsOn": "CopyZipsRole",
            "Type": "AWS::CloudFormation::CustomResource",
            "Properties": {
                "ServiceToken": {
                    "Fn::GetAtt": [
                        "CopyZipsFunction",
                        "Arn"
                    ]
                },
                "DestBucket": {
                    "Ref": "LambdaZipsBucket"
                },
                "SourceBucket": {
                    "Ref": "QSS3BucketName"
                },
                "Prefix": {
                    "Ref": "QSS3KeyPrefix"
                },
                "Objects": [
                    "lambda.zip",
                    "shutdown.zip"
                ]
            }
        },
        "CopyZipsRole": {
            "Type": "AWS::IAM::Role",
            "Properties": {
                "AssumeRolePolicyDocument": {
                    "Version": "2012-10-17",
                    "Statement": [
                        {
                            "Effect": "Allow",
                            "Principal": {
                                "Service": "lambda.amazonaws.com"
                            },
                            "Action": "sts:AssumeRole"
                        }
                    ]
                },
                "ManagedPolicyArns": [
                    "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
                ],
                "Path": "/",
                "Policies": [
                    {
                        "PolicyName": "lambda-copier",
                        "PolicyDocument": {
                            "Version": "2012-10-17",
                            "Statement": [
                                {
                                    "Effect": "Allow",
                                    "Action": [
                                        "s3:GetObject"
                                    ],
                                    "Resource": [
                                        {
                                            "Fn::Sub": "arn:aws:s3:::${QSS3BucketName}/${QSS3KeyPrefix}*"
                                        }
                                    ]
                                },
                                {
                                    "Effect": "Allow",
                                    "Action": [
                                        "s3:PutObject",
                                        "s3:DeleteObject"
                                    ],
                                    "Resource": [
                                        {
                                            "Fn::Sub": "arn:aws:s3:::${LambdaZipsBucket}/${QSS3KeyPrefix}*"
                                        }
                                    ]
                                }
                            ]
                        }
                    }
                ]
            }
        },
        "CopyZipsFunction": {
            "DependsOn": "CopyZipsRole",
            "Type": "AWS::Lambda::Function",
            "Properties": {
                "Description": "Copies objects from a source S3 bucket to a destination",
                "Handler": "index.handler",
                "Runtime": "python2.7",
                "Role": {
                    "Fn::GetAtt": [
                        "CopyZipsRole",
                        "Arn"
                    ]
                },
                "Timeout": 240,
                "Code": {
                    "ZipFile": {
                        "Fn::Join": [
                            "\n",
                            [
                                "import json",
                                "import logging",
                                "import threading",
                                "import boto3",
                                "import cfnresponse",
                                "",
                                "",
                                "def copy_objects(source_bucket, dest_bucket, prefix, objects):",
                                "    s3 = boto3.client('s3')",
                                "    for o in objects:",
                                "        key = prefix + o",
                                "        copy_source = {",
                                "            'Bucket': source_bucket,",
                                "            'Key': key",
                                "        }",
                                "        s3.copy_object(CopySource=copy_source, Bucket=dest_bucket, Key=key)",
                                "",
                                "",
                                "def delete_objects(bucket, prefix, objects):",
                                "    s3 = boto3.client('s3')",
                                "    objects = {'Objects': [{'Key': prefix + o} for o in objects]}",
                                "    s3.delete_objects(Bucket=bucket, Delete=objects)",
                                "",
                                "",
                                "def timeout(event, context):",
                                "    logging.error('Execution is about to time out, sending failure response to CloudFormation')",
                                "    cfnresponse.send(event, context, cfnresponse.FAILED, {}, None)",
                                "",
                                "",
                                "def handler(event, context):",
                                "    # make sure we send a failure to CloudFormation if the function is going to timeout",
                                "    timer = threading.Timer((context.get_remaining_time_in_millis() / 1000.00) - 0.5, timeout, args=[event, context])",
                                "    timer.start()",
                                "",
                                "    print('Received event: %s' % json.dumps(event))",
                                "    status = cfnresponse.SUCCESS",
                                "    try:",
                                "        source_bucket = event['ResourceProperties']['SourceBucket']",
                                "        dest_bucket = event['ResourceProperties']['DestBucket']",
                                "        prefix = event['ResourceProperties']['Prefix']",
                                "        objects = event['ResourceProperties']['Objects']",
                                "        if event['RequestType'] == 'Delete':",
                                "            delete_objects(dest_bucket, prefix, objects)",
                                "        else:",
                                "            copy_objects(source_bucket, dest_bucket, prefix, objects)",
                                "    except Exception as e:",
                                "        logging.error('Exception: %s' % e, exc_info=True)",
                                "        status = cfnresponse.FAILED",
                                "    finally:",
                                "        timer.cancel()",
                                "        cfnresponse.send(event, context, status, {}, None)",
                                ""
                            ]
                        ]
                    }
                }
            }
        },
        "DeployTemplateRole": {
            "Type": "AWS::IAM::Role",
            "Properties": {
                "AssumeRolePolicyDocument": {
                    "Version": "2012-10-17",
                    "Statement": [
                        {
                            "Effect": "Allow",
                            "Principal": {
                                "Service": "lambda.amazonaws.com"
                            },
                            "Action": "sts:AssumeRole"
                        }
                    ]
                },
                "Path": "/",
                "Policies": [
                    {
                        "PolicyName": "git2cp-deploytemplate",
                        "PolicyDocument": {
                            "Version": "2012-10-17",
                            "Statement": [
                                {
                                    "Effect": "Allow",
                                    "Action": [
                                        "logs:CreateLogGroup",
                                        "logs:CreateLogStream",
                                        "logs:PutLogEvents"
                                    ],
                                    "Resource": [
                                        "arn:aws:logs:*:*:*"
                                    ]
                                },
                                {
                                  "Effect": "Allow",
                                  "Action": [
                                      "cloudformation:*"
                                  ],
                                  "Resource": "*"
                                },
                                {
                                  "Effect": "Allow",
                                  "Action": [
                                      "*"
                                  ],
                                  "Resource": "*"
                                }
                            ]
                        }
                    }
                ]
            }
        },
        "DeployTemplateLambda": {
            "DependsOn": "CopyZips",
            "Type": "AWS::Lambda::Function",
            "Properties": {
                "Handler": "lambda_function.lambda_handler",
                "MemorySize": "128",
                "Role": {
                    "Fn::GetAtt": [
                        "DeployTemplateRole",
                        "Arn"
                    ]
                },
                "Runtime": "python2.7",
                "Timeout": "300",
                "Code": {
                    "S3Bucket": {
                        "Ref": "LambdaZipsBucket"
                    },
                    "S3Key": {
                        "Fn::Sub": "${QSS3KeyPrefix}lambda.zip"
                    }
                }
            }
        },
        "WebHookRole": {
            "Type": "AWS::IAM::Role",
            "Properties": {
                "AssumeRolePolicyDocument": {
                    "Version": "2012-10-17",
                    "Statement": [
                        {
                            "Effect": "Allow",
                            "Principal": {
                                "Service": "apigateway.amazonaws.com"
                            },
                            "Action": "sts:AssumeRole"
                        }
                    ]
                },
                "Path": "/",
                "ManagedPolicyArns": [
                    "arn:aws:iam::aws:policy/service-role/AmazonAPIGatewayPushToCloudWatchLogs"
                ],
                "Policies": [
                    {
                        "PolicyName": "git2cp-webhook",
                        "PolicyDocument": {
                            "Version": "2012-10-17",
                            "Statement": [
                                {
                                    "Effect": "Allow",
                                    "Action": [
                                        "lambda:InvokeAsync",
                                        "lambda:InvokeFunction"
                                    ],
                                    "Resource": [
                                        {
                                            "Fn::GetAtt": [
                                                "DeployTemplateLambda",
                                                "Arn"
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
                    }
                ]
            }
        },
        "WebHookApi": {
            "Type": "AWS::ApiGateway::RestApi",
            "Properties": {
                "Body": {
                    "swagger": "2.0",
                    "info": {
                        "version": "2016-07-26T07:34:38Z",
                        "title": {
                            "Fn::Join": [
                                "",
                                [
                                    "G2CP-",
                                    {
                                        "Ref": "AWS::StackName"
                                    }
                                ]
                            ]
                        }
                    },
                    "schemes": [
                        "https"
                    ],
                    "paths": {
                        "/deploytemplate": {
                            "post": {
                                "consumes": [
                                    "application/json"
                                ],
                                "produces": [
                                    "application/json"
                                ],
                                "responses": {
                                    "200": {
                                        "description": "200 response",
                                        "schema": {
                                            "$ref": "#/definitions/Empty"
                                        }
                                    }
                                },
                                "x-amazon-apigateway-integration": {
                                    "type": "aws",
                                    "credentials": {
                                        "Fn::GetAtt": [
                                            "WebHookRole",
                                            "Arn"
                                        ]
                                    },
                                    "responses": {
                                        "default": {
                                            "statusCode": "200"
                                        }
                                    },
                                    "requestParameters": {
                                        "integration.request.header.X-Amz-Invocation-Type": "'Event'"
                                    },
                                    "passthroughBehavior": "when_no_templates",
                                    "httpMethod": "POST",
                                    "uri": {
                                        "Fn::Join": [
                                            "",
                                            [
                                                "arn:aws:apigateway:",
                                                {
                                                    "Ref": "AWS::Region"
                                                },
                                                ":lambda:path//2015-03-31/functions/",
                                                {
                                                    "Fn::GetAtt": [
                                                        "DeployTemplateLambda",
                                                        "Arn"
                                                    ]
                                                },
                                                "/invocations"
                                            ]
                                        ]
                                    }
                                }
                            }
                        }
                    },
                    "securityDefinitions": {
                        "sigv4": {
                            "type": "apiKey",
                            "name": "Authorization",
                            "in": "header",
                            "x-amazon-apigateway-authtype": "awsSigv4"
                        }
                    },
                    "definitions": {
                        "Empty": {
                            "type": "object"
                        }
                    }
                }
            }
        },
        "WebHookApiDeployment": {
            "Type": "AWS::ApiGateway::Deployment",
            "Properties": {
                "RestApiId": {
                    "Ref": "WebHookApi"
                },
                "StageName": "DummyStage"
            }
        },
        "WebHookApiProdStage": {
            "Type": "AWS::ApiGateway::Stage",
            "Properties": {
                "DeploymentId": {
                    "Ref": "WebHookApiDeployment"
                },
                "RestApiId": {
                    "Ref": "WebHookApi"
                },
                "StageName": "Prod",
            }
        },
        "CustomDomainCertificate": {
            "Condition": "UseCustomDomain",
            "Type": "AWS::CertificateManager::Certificate",
            "Properties": {
                "DomainName": {
                    "Ref": "CustomDomainName"
                }
            }
        },
        "WebHookApiCustomDomainName": {
            "Condition": "UseCustomDomain",
            "Type": "AWS::ApiGateway::DomainName",
            "Properties": {
                "CertificateArn": {
                    "Ref": "CustomDomainCertificate"
                },
                "DomainName": {
                    "Ref": "CustomDomainName"
                }
            }
        },
        "WebHookApiCustomDomainNameMapping": {
            "Condition": "UseCustomDomain",
            "Type": "AWS::ApiGateway::BasePathMapping",
            "Properties": {
                "DomainName": {
                    "Ref": "CustomDomainName"
                },
                "RestApiId": {
                    "Ref": "WebHookApi"
                }
            }
        },
        "ShutdownLambdaRole": {
            "Type": "AWS::IAM::Role",
            "Properties": {
                "AssumeRolePolicyDocument": {
                    "Version": "2012-10-17",
                    "Statement": [
                        {
                            "Effect": "Allow",
                            "Principal": {
                                "Service": "lambda.amazonaws.com"
                            },
                            "Action": "sts:AssumeRole"
                        }
                    ]
                },
                "Path": "/",
                "Policies": [
                    {
                        "PolicyName": "git2cp-destroytemplate",
                        "PolicyDocument": {
                            "Version": "2012-10-17",
                            "Statement": [
                                {
                                    "Effect": "Allow",
                                    "Action": "*",
                                    "Resource": "*"
                                }
                            ]
                        }
                    }
                ]
            }
        },
        "ShutdownLambda": {
            "DependsOn": "CopyZips",
            "Type": "AWS::Lambda::Function",
            "Properties": {
                "Handler": "shutdown.lambda_handler",
                "MemorySize": "128",
                "Role": {
                    "Fn::GetAtt": [
                        "ShutdownLambdaRole",
                        "Arn"
                    ]
                },
                "Runtime": "python2.7",
                "Timeout": "300",
                "Code": {
                    "S3Bucket": {
                        "Ref": "LambdaZipsBucket"
                    },
                    "S3Key": {
                        "Fn::Sub": "${QSS3KeyPrefix}shutdown.zip"
                    }
                }
            }
        },
        "ShutdownApiRole": {
            "Type": "AWS::IAM::Role",
            "Properties": {
                "AssumeRolePolicyDocument": {
                    "Version": "2012-10-17",
                    "Statement": [
                        {
                            "Effect": "Allow",
                            "Principal": {
                                "Service": "apigateway.amazonaws.com"
                            },
                            "Action": "sts:AssumeRole"
                        }
                    ]
                },
                "Path": "/",
                "ManagedPolicyArns": [
                    "arn:aws:iam::aws:policy/service-role/AmazonAPIGatewayPushToCloudWatchLogs"
                ],
                "Policies": [
                    {
                        "PolicyName": "git2cp-destroytemplate",
                        "PolicyDocument": {
                            "Version": "2012-10-17",
                            "Statement": [
                                {
                                    "Effect": "Allow",
                                    "Action": [
                                        "lambda:InvokeAsync",
                                        "lambda:InvokeFunction"
                                    ],
                                    "Resource": [
                                        {
                                            "Fn::GetAtt": [
                                                "ShutdownLambda",
                                                "Arn"
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
                    }
                ]
            }
        },
        "ShutdownApi": {
            "Type": "AWS::ApiGateway::RestApi",
            "Properties": {
                "Body": {
                    "swagger": "2.0",
                    "info": {
                        "version": "2016-07-26T07:34:38Z",
                        "title": {
                            "Fn::Join": [
                                "",
                                [
                                    "G2CP-",
                                    {
                                        "Ref": "AWS::StackName"
                                    }
                                ]
                            ]
                        }
                    },
                    "schemes": [
                        "https"
                    ],
                    "paths": {
                        "/destroytemplate": {
                            "post": {
                                "consumes": [
                                    "application/json"
                                ],
                                "produces": [
                                    "application/json"
                                ],
                                "responses": {
                                    "200": {
                                        "description": "200 response",
                                        "schema": {
                                            "$ref": "#/definitions/Empty"
                                        }
                                    }
                                },
                                "x-amazon-apigateway-integration": {
                                    "type": "aws",
                                    "credentials": {
                                        "Fn::GetAtt": [
                                            "ShutdownApiRole",
                                            "Arn"
                                        ]
                                    },
                                    "responses": {
                                        "default": {
                                            "statusCode": "200"
                                        }
                                    },
                                    "requestParameters": {
                                        "integration.request.header.X-Amz-Invocation-Type": "'Event'"
                                    },
                                    "passthroughBehavior": "when_no_templates",
                                    "httpMethod": "POST",
                                    "uri": {
                                        "Fn::Join": [
                                            "",
                                            [
                                                "arn:aws:apigateway:",
                                                {
                                                    "Ref": "AWS::Region"
                                                },
                                                ":lambda:path//2015-03-31/functions/",
                                                {
                                                    "Fn::GetAtt": [
                                                        "ShutdownLambda",
                                                        "Arn"
                                                    ]
                                                },
                                                "/invocations"
                                            ]
                                        ]
                                    }
                                }
                            }
                        }
                    },
                    "securityDefinitions": {
                        "sigv4": {
                            "type": "apiKey",
                            "name": "Authorization",
                            "in": "header",
                            "x-amazon-apigateway-authtype": "awsSigv4"
                        }
                    },
                    "definitions": {
                        "Empty": {
                            "type": "object"
                        }
                    }
                }
            }
        },
        "ShutdownApiDeployment": {
            "Type": "AWS::ApiGateway::Deployment",
            "Properties": {
                "RestApiId": {
                    "Ref": "ShutdownApi"
                },
                "StageName": "DummyStage"
            }
        },
        "ShutdownApiProdStage": {
            "Type": "AWS::ApiGateway::Stage",
            "Properties": {
                "DeploymentId": {
                    "Ref": "ShutdownApiDeployment"
                },
                "RestApiId": {
                    "Ref": "ShutdownApi"
                },
                "StageName": "Prod",
            }
        }
    },
    "Outputs": {
        "ShutdownApi": {
            "Value": {
                "Fn::Join": [
                    "",
                    [
                        "https://",
                        {
                            "Fn::Join": [
                                "",
                                [
                                    {
                                        "Ref": "ShutdownApi"
                                    },
                                    ".execute-api.",
                                    {
                                        "Ref": "AWS::Region"
                                    },
                                    ".amazonaws.com"
                                ]
                            ]
                        },
                        "/",
                        {
                            "Ref": "ShutdownApiProdStage"
                        },
                        "/destroytemplate"
                    ]
                ]
            }
        },
        "DeployTemplateWebHookApi": {
            "Value": {
                "Fn::Join": [
                    "",
                    [
                        "https://",
                        {
                            "Fn::If": [
                                "UseCustomDomain",
                                {
                                    "Ref": "CustomDomainName"
                                },
                                {
                                    "Fn::Join": [
                                        "",
                                        [
                                            {
                                                "Ref": "WebHookApi"
                                            },
                                            ".execute-api.",
                                            {
                                                "Ref": "AWS::Region"
                                            },
                                            ".amazonaws.com"
                                        ]
                                    ]
                                }
                            ]
                        },
                        "/",
                        {
                            "Ref": "WebHookApiProdStage"
                        },
                        "/deploytemplate"
                    ]
                ]
            }
        }
    }
}
