import boto3


def lambda_handler(event, context):
    client = boto3.client('cloudformation')
    request = client.delete_stack(
        StackName='GradsTestEnv',
    )
    return request
