AWSTemplateFormatVersion: 2010-09-09
Description: >-
  Builds a VPC w/ INET Gateway and a public subnet with EC2 instances for Jenkins Distributed Build.

Parameters :
  KeyName :
    Description : 'Name of an existing EC2 KeyPair to enable SSH access to the instance'
    Type : 'AWS::EC2::KeyPair::KeyName'
    Default : 'final_assessment'
    ConstraintDescription : 'Must be the name of an existing EC2 KeyPair'
  MasterInstanceType :
    Description : 'Master EC2 instance type'
    Type : 'String'
    Default : 't2.micro'
    ConstraintDescription : 'Must be a valid EC2 instance type'
  SlaveInstanceType :
   Description : 'Slave EC2 instance type'
   Type : 'String'
   Default : 't2.small'
   ConstraintDescription : 'Must be a valid EC2 instance type'
  MasterAMI :
    Description : 'AMI for the EC2 instance'
    Type : 'String'
    Default : 'ami-83fb2bfe'
    ConstraintDescription : 'Must be a valid AMI for the region'
  SlaveAMI :
    Description : 'AMI for the EC2 instance'
    Type : 'String'
    Default : 'ami-1853ac65'
    ConstraintDescription : 'Must be a valid AMI for the region'

Resources:
  VPC:
    Type: 'AWS::EC2::VPC'
    Properties:
      CidrBlock: 10.104.0.0/16
      Tags:
        - Key: Application
          Value: !Ref 'AWS::StackName'
        - Key: Network
          Value: Public
  PublicSubnet:
    Type: 'AWS::EC2::Subnet'
    Properties:
      VpcId: !Ref VPC
      CidrBlock: 10.104.0.0/24
      AvailabilityZone: !Select
        - '0'
        - !GetAZs
          Ref: 'AWS::Region'
      Tags:
        - Key: Application
          Value: !Ref 'AWS::StackName'
        - Key: Network
          Value: Public
  InternetGateway:
    Type: 'AWS::EC2::InternetGateway'
    Properties:
      Tags:
        - Key: Application
          Value: !Ref 'AWS::StackName'
        - Key: Network
          Value: Public
  AttachGateway:
    Type: 'AWS::EC2::VPCGatewayAttachment'
    Properties:
      VpcId: !Ref VPC
      InternetGatewayId: !Ref InternetGateway
  PublicRouteTable:
    Type: 'AWS::EC2::RouteTable'
    Properties:
      VpcId: !Ref VPC
      Tags:
        - Key: Application
          Value: !Ref 'AWS::StackName'
        - Key: Network
          Value: Public
  PublicRoute:
    Type: 'AWS::EC2::Route'
    Properties:
      RouteTableId: !Ref PublicRouteTable
      DestinationCidrBlock: 0.0.0.0/0
      GatewayId: !Ref InternetGateway
  myDNSRecord:
    Type: AWS::Route53::RecordSet
    Properties:
      HostedZoneId: Z2U00Q95U7EKEA
      Name: jenkinsmaster.grads.al-labs.co.uk.
      Type: A
      TTL: '300'
      ResourceRecords:
      - !GetAtt
        - jenkinsMasterInstance
        - PublicIp
  PublicSubnetRouteTableAssociation:
    Type: 'AWS::EC2::SubnetRouteTableAssociation'
    Properties:
      SubnetId: !Ref PublicSubnet
      RouteTableId: !Ref PublicRouteTable
  jenkinsMasterInstance :
    Type : 'AWS::EC2::Instance'
    Properties :
      IamInstanceProfile: 'jenkinsadmin'
      InstanceType : !Ref MasterInstanceType
      KeyName : !Ref KeyName
      ImageId : !Ref MasterAMI
      NetworkInterfaces:
          - AssociatePublicIpAddress: true
            DeleteOnTermination: true
            DeviceIndex: '0'
            SubnetId: !Ref PublicSubnet
            GroupSet:
                - !Ref jenkinsSecurityGroup
      UserData:
        Fn::Base64:
          !Sub |
            #!/bin/bash -xe
            aws s3 cp s3://gradstestbucket/jenkinsinfra/jenkins_shell_mt.sh /var/tmp/jenkins_shell_mt.sh
            chmod +x /var/tmp/jenkins_shell_mt.sh
            /var/tmp/jenkins_shell_mt.sh
      Tags :
          - Key: 'Name'
            Value: 'jenkinsMasterInstance'
  jenkinsSlaveInstance1:
    DependsOn: 'jenkinsMasterInstance'
    Type : 'AWS::EC2::Instance'
    Properties :
      IamInstanceProfile: 'jenkinsadmin'
      InstanceType : !Ref SlaveInstanceType
      KeyName : !Ref KeyName
      ImageId : !Ref SlaveAMI
      NetworkInterfaces:
          - AssociatePublicIpAddress: true
            DeleteOnTermination: true
            DeviceIndex: '0'
            SubnetId: !Ref PublicSubnet
            GroupSet:
                  - !Ref jenkinsSecurityGroup
      UserData:
        Fn::Base64:
          !Sub |
            #!/bin/bash -xe
            aws s3 cp s3://gradstestbucket/jenkinsinfra/jenkins_shell_s1.sh /var/tmp/jenkins_shell_s1.sh
            chmod +x /var/tmp/jenkins_shell_s1.sh
            /var/tmp/jenkins_shell_s1.sh
      Tags :
          - Key: 'Name'
            Value: 'jenkinsSlave1'
  jenkinsSlaveInstance2:
    DependsOn: 'jenkinsMasterInstance'
    Type : 'AWS::EC2::Instance'
    Properties :
      IamInstanceProfile: 'jenkinsadmin'
      InstanceType : !Ref SlaveInstanceType
      KeyName : !Ref KeyName
      ImageId : !Ref SlaveAMI
      NetworkInterfaces:
          - AssociatePublicIpAddress: true
            DeleteOnTermination: true
            DeviceIndex: '0'
            SubnetId : !Ref PublicSubnet
            GroupSet:
                  - !Ref jenkinsSecurityGroup
      UserData:
        Fn::Base64:
          !Sub |
            #!/bin/bash -xe
            aws s3 cp s3://gradstestbucket/jenkinsinfra/jenkins_shell_s1.sh /var/tmp/jenkins_shell_s1.sh
            chmod +x /var/tmp/jenkins_shell_s1.sh
            /var/tmp/jenkins_shell_s1.sh
      Tags :
          - Key: 'Name'
            Value: 'jenkinsSlave2'
  jenkinsSecurityGroup:
    Type: "AWS::EC2::SecurityGroup"
    Properties:
        GroupName: final_assessment
        GroupDescription: Security Group for Jenkins masters and slaves
        SecurityGroupIngress:
            - IpProtocol: tcp
              FromPort: '22'
              ToPort: '22'
              CidrIp: '0.0.0.0/0'
            - IpProtocol: tcp
              FromPort: '8080'
              ToPort: '8080'
              CidrIp: '0.0.0.0/0'
        Tags:
            - Key: Name
              Value: Jenkins-SG
        VpcId: !Ref VPC
Outputs:
  VpcId:
    Value: !Ref VPC
    Description: VPC ID of newly created VPC
  PublicSubnet:
    Value: !Ref PublicSubnet
    Description: Public Subnet for Jenkins servers
  GroupId:
    Value: !Ref jenkinsSecurityGroup
    Description: GroupId for EC2 created security group.
