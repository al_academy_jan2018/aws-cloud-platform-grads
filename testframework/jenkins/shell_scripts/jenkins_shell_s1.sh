#!/bin/bash

# Install jq on slave 1
yum -y install epel-release
yum -y install jq

# install aws cli on slave 1
sudo pip install awscli --upgrade
/usr/local/bin/pip install awscli --upgrade

mkdir /home/ec2-user/.aws/
aws s3 cp s3://gradstestbucket/Encrypted_AwsUserCred /home/ec2-user/.aws/
aws kms decrypt --region eu-west-2 --ciphertext-blob fileb:///home/ec2-user/.aws/Encrypted_AwsUserCred --output text --query Plaintext | base64 --decode > credentials

cat > /home/ec2-user/.aws/config<<_END_
[default]
region = us-east-1
output = json
_END_


# Set public IPs of master and private + public IPs of 2 slaves as variables for later
master_pubip=$(aws ec2 describe-instances --region us-east-1 --filters "Name=tag:Name,Values=jenkinsMasterInstance" | jq -r '.Reservations[].Instances[] | select(.State.Name == "running") | .PublicIpAddress')

slave1_privip=$(aws ec2 describe-instances --region us-east-1 --filters "Name=tag:Name,Values=jenkinsSlave1" | jq -r '.Reservations[].Instances[] | select(.State.Name == "running") | .PrivateIpAddress')
until [[ -n $slave1_privip ]]
do
	sleep 10
	slave1_privip=$(aws ec2 describe-instances --region us-east-1 --filters "Name=tag:Name,Values=jenkinsSlave1" | jq -r '.Reservations[].Instances[] | select(.State.Name == "running") | .PrivateIpAddress')
done

slave2_privip=$(aws ec2 describe-instances --region us-east-1 --filters "Name=tag:Name,Values=jenkinsSlave2" | jq -r '.Reservations[].Instances[] | select(.State.Name == "running") | .PrivateIpAddress')
until [[ -n $slave2_privip ]]
do
	sleep 10
	slave2_privip=$(aws ec2 describe-instances --region us-east-1 --filters "Name=tag:Name,Values=jenkinsSlave2" | jq -r '.Reservations[].Instances[] | select(.State.Name == "running") | .PrivateIpAddress')
done



# copy public key from s3 bucket to home directory
aws s3 cp s3://gradstestbucket/master_1.pub /tmp/
pub_key=$(cat /tmp/master_1.pub)
echo "${pub_key}" >> /home/ec2-user/.ssh/authorized_keys

# Copy required Jenkins jar file from master to slave
scp https://${master_privip}:8080/jnlpJars/jenkins-cli.jar ec2-user@${slave1_privip}:

# Install Java 1.8 and uninstall Java 1.7
yum -y install java-1.8.0
yum -y remove java-1.7.0

# install git on slave 1
yum -y install git

# Installing jq on slave 1
yum -y install epel-release
yum -y install jq

# Connect slave 1 to master
java -jar jenkins-cli.jar -s ${master_pubip}:8080 create-node slave_1

# Copy encrypted keys and config file from s3 bucket to .ssh folder
# aws s3 cp s3://gradstestbucket/Encrypted_codecommit_rsa /home/ec2-user/.ssh/
# aws s3 cp s3://gradstestbucket/Encrypted_codecommit_rsa.pub /home/ec2-user/.ssh/
# aws s3 cp s3://gradstestbucket/Encrypted_config /home/ec2-user/.ssh/

aws s3 cp s3://gradstestbucket/git_ssh/id_rsa.enc /home/ec2-user/.ssh/
aws s3 cp s3://gradstestbucket/git_ssh/id_rsa.pub.enc /home/ec2-user/.ssh/
aws s3 cp s3://gradstestbucket/git_ssh/config.enc /home/ec2-user/.ssh/

#change directory
cd /home/ec2-user/.ssh/

# Decrypt each file
# aws kms decrypt --region eu-west-2 --ciphertext-blob fileb:///home/ec2-user/.ssh/Encrypted_codecommit_rsa --output text --query Plaintext | base64 --decode > codecommit_rsa
# aws kms decrypt --region eu-west-2 --ciphertext-blob fileb:///home/ec2-user/.ssh/Encrypted_codecommit_rsa.pub --output text --query Plaintext | base64 --decode > codecommit_rsa.pub
# aws kms decrypt --region eu-west-2 --ciphertext-blob fileb:///home/ec2-user/.ssh/Encrypted_config --output text --query Plaintext | base64 --decode > config
aws kms decrypt --region eu-west-2 --ciphertext-blob fileb:///home/ec2-user/.ssh/id_rsa.enc --output text --query Plaintext | base64 --decode > /home/ec2-user/.ssh/id_rsa
chown ec2-user:ec2-user id_rsa
chmod 600 id_rsa
aws kms decrypt --region eu-west-2 --ciphertext-blob fileb:///home/ec2-user/.ssh/id_rsa.pub.enc --output text --query Plaintext | base64 --decode > /home/ec2-user/.ssh/id_rsa.pub
chmod 600 id_rsa.pub
chown ec2-user:ec2-user id_rsa.pub
aws kms decrypt --region eu-west-2 --ciphertext-blob fileb:///home/ec2-user/.ssh/config.enc --output text --query Plaintext | base64 --decode > /home/ec2-user/.ssh/config
chmod 600 config
chown ec2-user:ec2-user config

rm -rf /home/ec2-user/.ssh/known_hosts
