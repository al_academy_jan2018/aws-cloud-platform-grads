#!/bin/bash

pip install awscli --upgrade

master_pubip=$(aws ec2 describe-instances --region us-east-1 --filters "Name=tag:Name,Values=jenkinsMasterInstance" | jq -r '.Reservations[].Instances[] | select(.State.Name == "running") | .PublicIpAddress')

slave1_privip=$(aws ec2 describe-instances --region us-east-1 --filters "Name=tag:Name,Values=jenkinsSlave1" | jq -r '.Reservations[].Instances[] | select(.State.Name == "running") | .PrivateIpAddress')
until [[ -n $slave1_privip ]]
do
	sleep 10
	slave1_privip=$(aws ec2 describe-instances --region us-east-1 --filters "Name=tag:Name,Values=jenkinsSlave1" | jq -r '.Reservations[].Instances[] | select(.State.Name == "running") | .PrivateIpAddress')
done

slave2_privip=$(aws ec2 describe-instances --region us-east-1 --filters "Name=tag:Name,Values=jenkinsSlave2" | jq -r '.Reservations[].Instances[] | select(.State.Name == "running") | .PrivateIpAddress')
until [[ -n $slave2_privip ]]
do
	sleep 10
	slave2_privip=$(aws ec2 describe-instances --region us-east-1 --filters "Name=tag:Name,Values=jenkinsSlave2" | jq -r '.Reservations[].Instances[] | select(.State.Name == "running") | .PrivateIpAddress')
done

cat >/var/lib/jenkins/nodes/Slave_1/config.xml <<_END_
<?xml version='1.1' encoding='UTF-8'?>
<slave>
  <name>Slave_1</name>
  <description></description>
  <remoteFS>/home/ec2-user</remoteFS>
  <numExecutors>2</numExecutors>
  <mode>NORMAL</mode>
  <retentionStrategy class="hudson.slaves.RetentionStrategy$Always"/>
  <launcher class="hudson.plugins.sshslaves.SSHLauncher" plugin="ssh-slaves@1.26">
    <host>${slave1_privip}</host>
    <port>22</port>
    <credentialsId>ec2_worker</credentialsId>
    <maxNumRetries>0</maxNumRetries>
    <retryWaitTime>0</retryWaitTime>
    <sshHostKeyVerificationStrategy class="hudson.plugins.sshslaves.verifiers.NonVerifyingKeyVerificationStrategy"/>
  </launcher>
  <label></label>
  <nodeProperties/>
</slave>
_END_

cat >/var/lib/jenkins/nodes/Slave_2/config.xml <<_END_
<?xml version='1.1' encoding='UTF-8'?>
<slave>
  <name>Slave_2</name>
  <description></description>
  <remoteFS>/home/ec2-user</remoteFS>
  <numExecutors>2</numExecutors>
  <mode>NORMAL</mode>
  <retentionStrategy class="hudson.slaves.RetentionStrategy$Always"/>
  <launcher class="hudson.plugins.sshslaves.SSHLauncher" plugin="ssh-slaves@1.26">
    <host>${slave2_privip}</host>
    <port>22</port>
    <credentialsId>ec2_worker</credentialsId>
    <maxNumRetries>0</maxNumRetries>
    <retryWaitTime>0</retryWaitTime>
    <sshHostKeyVerificationStrategy class="hudson.plugins.sshslaves.verifiers.NonVerifyingKeyVerificationStrategy"/>
  </launcher>
  <label></label>
  <nodeProperties/>
</slave>
_END_

rm -rf /var/lib/jenkins/.ssh/known_hosts

if rpm -qa | grep jenkins
then
	service jenkins restart
fi
