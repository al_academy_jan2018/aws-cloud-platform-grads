#!/bin/bash

# Installing jq on master
yum -y install epel-release
yum -y install jq

# install AWS CLI and git on master
pip install awscli --upgrade
yum -y install git
echo "awscli and git installed"

# Install Java 1.8 and uninstall Java 1.7
yum -y install java-1.8.0
yum -y remove java-1.7.0
echo "java stuff done"
# Install Jenkins on the master
wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat/jenkins.repo

rpm --import https://jenkins-ci.org/redhat/jenkins-ci.org.key

yum -y install jenkins
echo "jenkins installed"

# Start Jenkins
service jenkins start
echo "jenkins started"
read -r password < /var/lib/jenkins/secrets/initialAdminPassword
curl -v --data "from=%2f&j_username=admin&j_password=$password" -X POST "http://jenkinsmaster.grads.al-labs.co.uk:8080/j_acegi_security_check"
# Copy the private key from the s3 bucket to the .ssh folder
aws s3 cp s3://gradstestbucket/master_1 ~/var/lib/jenkins/.ssh/

# Re-populate home
aws s3 cp s3://gradstestbucket/jenkinsbackup/jenkins.tar.gz .
tar xzf jenkinsbackup.tar.gz -P /var/lib/jenkins/ --exclude='/var/lib/jenkins/workspace'
