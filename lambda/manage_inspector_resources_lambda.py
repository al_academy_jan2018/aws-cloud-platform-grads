import json
import boto3
import cfnresponse
from time import sleep

client = boto3.client('inspector')

def handler(event, context):
    if event['RequestType'] == 'Update':
        cfnresponse.send(event, context, cfnresponse.SUCCESS, {})
    try:
        if event['RequestType'] == 'Create':
            resourceArns = prepare_inspector_resources(event)
            print('Resource creation finished successfully')
            print(resourceArns)
            cfnresponse.send(event, context, cfnresponse.SUCCESS, resourceArns)
        if event['RequestType'] == 'Delete':
            cleanup_inspector_resources(event)
            print('Resource deletion finished successfully')
            cfnresponse.send(event, context, cfnresponse.SUCCESS, {})
    except Exception as e:
        print('Function failed due to exception.')
        print(e)
        response_data = {'Reason': str(e)}
        cfnresponse.send(event, context, cfnresponse.FAILED, response_data)

def prepare_inspector_resources(event):
    client.register_cross_account_access_role(
        roleArn=event['ResourceProperties']['InspectorRole']
    )

    create_group_response = client.create_resource_group(
        resourceGroupTags=event['ResourceProperties']['ResourceGroupTags']
    )

    create_target_response = client.create_assessment_target(
        assessmentTargetName=event['ResourceProperties']['AssessmentTargetName'],
        resourceGroupArn=create_group_response['resourceGroupArn']
    )

    create_template_response = client.create_assessment_template(
        assessmentTargetArn=create_target_response['assessmentTargetArn'],
        assessmentTemplateName=event['ResourceProperties']['AssessmentTemplateName'],
        durationInSeconds=int(event['ResourceProperties']['AssessmentDuration']),
        rulesPackageArns=event['ResourceProperties']['RulesPackageArns']
    )

    return(
        {
            'ResourceGroupArn': create_group_response['resourceGroupArn'],
            'AssessmentTargetArn': create_target_response['assessmentTargetArn'],
            'AssessmentTemplateArn': create_template_response['assessmentTemplateArn']
        }
    )

ASSESSMENT_DELETABLE_STATES = [
    'CREATED', 'FAILED', 'ERROR', 'COMPLETED', 'COMPLETED_WITH_ERRORS', 'CANCELED']

def delete_assessment_run(runArn):
    client.stop_assessment_run(assessmentRunArn = runArn)

    state = None

    while state not in ASSESSMENT_DELETABLE_STATES:
        response = client.describe_assessment_runs(assessmentRunArns=[runArn])
        if response['assessmentRuns']:
            state = response['assessmentRuns'][0]['state']
        sleep(3)

    client.delete_assessment_run(assessmentRunArn = runArn)
    return('Deleted assessment run with ARN: {}'.format(runArn))


def cleanup_inspector_resources(event):
    list_templates_response = client.list_assessment_templates(
        filter = {
            'namePattern': event['ResourceProperties']['AssessmentTemplateName']
        },
        maxResults = 1
    )
    if list_templates_response['assessmentTemplateArns']:
        template = list_templates_response['assessmentTemplateArns'][0]
        runs = client.list_assessment_runs(
            assessmentTemplateArns=[template]
        )
        print(list(map(delete_assessment_run, runs['assessmentRunArns'])))
        client.delete_assessment_template(assessmentTemplateArn=template)
        print('Deleted assessment template with ARN: {}'.format(template))

    list_targets_response = client.list_assessment_targets(
        filter={
            'assessmentTargetNamePattern': event['ResourceProperties']['AssessmentTargetName']
        },
        maxResults = 1
    )
    if list_targets_response['assessmentTargetArns']:
        client.delete_assessment_target(assessmentTargetArn = list_targets_response['assessmentTargetArns'][0])
        print('Deleted assessment target with ARN: {}'.format(list_targets_response['assessmentTargetArns'][0]))
