var AWS = require('aws-sdk');
var response = require('cfn-response');
AWS.config.update({region: 'us-east-1'});
exports.handler = function (event, context) {
  try {
    var acm = new AWS.ACM();
    if(event.RequestType == 'Delete') {
      console.log('Delete Called');
      response.send(event, context, response.SUCCESS);
    }
    else{
      var params = {
        DomainName: event.ResourceProperties.DomainName,
        DomainValidationOptions: [
          {
            DomainName: event.ResourceProperties.DomainName,
            ValidationDomain: event.ResourceProperties.ValidationDomain
          },
        ],
        SubjectAlternativeNames: event.ResourceProperties.SubjectAlternativeNames
      };
      acm.requestCertificate(params, function(err, data) {
        if (err){
          console.log(err, err.stack); // an error occurred
          response.send(event, context, response.FAILED);
        }
        else{
          console.log(data);           // successful response
          response.send(event, context, response.SUCCESS, data);
        }
      });
    }
  }
  catch (err) {
    console.log('General Error.');
    console.log(err);
    response.send(event, context, response.FAILED);
  }
};
