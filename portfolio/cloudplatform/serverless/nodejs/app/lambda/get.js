const AWS = require('aws-sdk');
const dynamo = new AWS.DynamoDB();
exports.handler = function (event, context, callback) {
  console.log('Event:',JSON.stringify(event));
  const done = (err, res) => callback(null, {
    statusCode: err ? '400' : '200',
    body: err ? err.message : res,
    headers: {
      'Content-Type': 'application/json',
    },
  });
  var tableName=process.env.TableName;
  dynamo.scan({ TableName: tableName }, function (err,res) {
    if (err){
      console.log('Error Getting Items from Database:',JSON.stringify(err));
    }
    else{
      if(res.Items.length >0){
        var new_res=[];
        res.Items.forEach(function(obj) {
          var temp={};
          temp._id=obj._id.S;
          temp.text=obj.text.S;
          new_res.push(temp);
        });
        res=new_res;
      }
      else{
        res={};
      }
    }
    done(err,res);
  });
};
