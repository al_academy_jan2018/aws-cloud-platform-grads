swagger: '2.0'
info:
  description: This is a sample Todo Application server.
  version: 1.0.0
  title: Todo WebApp
  contact:
    email: info@automationlogic.com
  license:
    name: Apache 2.0
    url: 'http://www.apache.org/licenses/LICENSE-2.0.html'
basePath: /v1
schemes:
  - http
consumes:
  - application/json
produces:
  - application/json
tags:
  - name: Todo
    description: Everything about your Todo Tasks
    externalDocs:
      description: Find out more about the Automation Logic Cloud Platform
      url: 'https://bitbucket.org/automationlogic/aws-cloud-platform/overview'
paths:
  /todo:
    get:
      tags:
        - todo
      summary: Returns a list of todos
      description: Returns a map of todos
      operationId: getTodos
      produces:
        - application/json
      parameters: []
      responses:
        '200':
          description: successful operation
          headers:
            Access-Control-Allow-Origin:
              type: "string"
      x-amazon-apigateway-integration:
        type: aws
        uri: 'arn:aws:apigateway:$AWSRegion:lambda:path/2015-03-31/functions/$GetLambdaArn/invocations'
        # Although the API Endpoint is for a GET Method. This value represents calling the lambda which needs to be done via a POST.
        #  http://docs.aws.amazon.com/lambda/latest/dg/API_Invoke.html
        httpMethod: POST
        responses:
          default:
            statusCode: '200'
            responseTemplates:
              application/json: $input.json('$.body')
            responseParameters:
              method.response.header.Access-Control-Allow-Origin: "'*'"
    options:
      summary: CORS support
      description: |
        Enable CORS by returning correct headers
      consumes:
        - application/json
      produces:
        - application/json
      tags:
        - CORS
      x-amazon-apigateway-integration:
        type: mock
        requestTemplates:
          application/json: |
            {
              "statusCode" : 200
            }
        responses:
          "default":
            statusCode: "200"
            responseParameters:
              method.response.header.Access-Control-Allow-Headers: "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token'"
              method.response.header.Access-Control-Allow-Methods: "'POST,GET,OPTIONS'"
              method.response.header.Access-Control-Allow-Origin: "'*'"
            responseTemplates:
              application/json: |
                {}
      responses:
        200:
          description: Default response for CORS method
          headers:
            Access-Control-Allow-Headers:
              type: "string"
            Access-Control-Allow-Methods:
              type: "string"
            Access-Control-Allow-Origin:
              type: "string"
    post:
      tags:
        - todo
      summary: Add a Todo
      description: ''
      operationId: addTodo
      produces:
        - application/json
      parameters:
        - in: body
          name: body
          description: Todo task added to list of Todos
          required: true
          schema:
            $ref: '#/definitions/AddTodo'
      responses:
        '200':
          description: successful operation
          headers:
            Access-Control-Allow-Origin:
              type: "string"
          schema:
            $ref: '#/definitions/EmptyResponse'
        '400':
          description: Invalid Todo
          headers:
            Access-Control-Allow-Origin:
              type: "string"
      x-amazon-apigateway-integration:
        type: aws
        uri: 'arn:aws:apigateway:$AWSRegion:lambda:path/2015-03-31/functions/$PostLambdaArn/invocations'
        httpMethod: POST
        requestTemplates:
          application/json: '{"body": $input.json(''$'')}'
        responses:
          default:
            statusCode: '200'
            responseTemplates:
              application/json: $input.json('$.body')
            responseParameters:
              method.response.header.Access-Control-Allow-Origin: "'*'"
  /todo/{_id}:
    options:
      summary: CORS support
      description: |
        Enable CORS by returning correct headers
      consumes:
        - application/json
      produces:
        - application/json
      tags:
        - CORS
      x-amazon-apigateway-integration:
        type: mock
        requestTemplates:
          application/json: |
            {
              "statusCode" : 200
            }
        responses:
          "default":
            statusCode: "200"
            responseParameters:
              method.response.header.Access-Control-Allow-Headers: "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token'"
              method.response.header.Access-Control-Allow-Methods: "'DELETE,OPTIONS'"
              method.response.header.Access-Control-Allow-Origin: "'*'"
            responseTemplates:
              application/json: |
                {}
      responses:
        200:
          description: Default response for CORS method
          headers:
            Access-Control-Allow-Headers:
              type: "string"
            Access-Control-Allow-Methods:
              type: "string"
            Access-Control-Allow-Origin:
              type: "string"
    delete:
      tags:
        - todo
      summary: Delete Todo task by ID
      description: For valid response try integer IDs with positive integer value.
      operationId: deleteTodo
      produces:
        - application/json
      parameters:
        - in: path
          name: _id
          description: ID of the todo task that needs to be deleted
          required: true
          type: "string"
      responses:
        '200':
          description: successful operation
          headers:
            Access-Control-Allow-Origin:
              type: "string"
          schema:
            $ref: '#/definitions/EmptyResponse'
        '400':
          description: Invalid ID supplied
          headers:
            Access-Control-Allow-Origin:
              type: "string"
        '404':
          description: Todo Task not found
          headers:
            Access-Control-Allow-Origin:
              type: "string"
      x-amazon-apigateway-integration:
        type: aws
        uri: 'arn:aws:apigateway:$AWSRegion:lambda:path/2015-03-31/functions/$DeleteLambdaArn/invocations'
        # Although the API Endpoint is for a GET Method. This value represents calling the lambda which needs to be done via a POST.
        #  http://docs.aws.amazon.com/lambda/latest/dg/API_Invoke.html
        httpMethod: POST
        passthroughBehavior: "never"
        requestTemplates:
          application/json: |
            #set($allParams = $input.params())
            {
              "params" : {
              #foreach($type in $allParams.keySet())
               #set($params = $allParams.get($type))
              "$type" : {
                #foreach($paramName in $params.keySet())
                "$paramName" : "$util.escapeJavaScript($params.get($paramName))"
                #if($foreach.hasNext),#end
                #end
                }
              #if($foreach.hasNext),#end
              #end
              }
            }
        responses:
          default:
            statusCode: '200'
            responseTemplates:
              application/json: $input.json('$.body')
            responseParameters:
              method.response.header.Access-Control-Allow-Origin: "'*'"
definitions:
  AddTodo:
    type: object
    properties:
      text:
        type: string
    required:
      - text
  EmptyResponse:
    type: object
externalDocs:
  description: Find out more about the Automation Logic Cloud Platform
  url: 'https://bitbucket.org/automationlogic/aws-cloud-platform/overview'
