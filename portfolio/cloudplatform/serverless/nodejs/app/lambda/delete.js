const AWS = require('aws-sdk');
const dynamo = new AWS.DynamoDB();
exports.handler = function (event, context, callback) {
  console.log('Event:',JSON.stringify(event));
  console.log('Context:',JSON.stringify(context));
  const done = (err, res) => callback(null, {
    statusCode: err ? '400' : '200',
    body: err ? err.message : res,
    headers: {
      'Content-Type': 'application/json',
    },
  });
  var tableName=process.env.TableName;
  var params = {};
  params.TableName=tableName;
  params.Key={};
  params.Key._id={S:event.params.path._id};
  dynamo.deleteItem(params, function (err,res) {
    if (err){
      console.log('Error Deleting Item from Database:',JSON.stringify(err));
    }
    done(err,res);
  });
};
