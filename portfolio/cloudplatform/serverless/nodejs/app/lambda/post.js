const AWS = require('aws-sdk');
const UUID = require('uuid/v1');
const dynamo = new AWS.DynamoDB();
exports.handler = function (event, context, callback) {
  console.log('Event:',JSON.stringify(event));
  const done = (err, res) => callback(null, {
    statusCode: err ? '400' : '200',
    body: err ? err.message : res,
    headers: {
      'Content-Type': 'application/json',
    },
  });
  var tableName=process.env.TableName;
  var params = {};
  params.TableName=tableName;
  params.Item={};
  params.Item._id={S:UUID()};
  params.Item.text={S:event.body.text};
  console.log(JSON.stringify(params));
  dynamo.putItem(params,function (err,res) {
    if (err){
      console.log('Error Adding Item in Database:',JSON.stringify(err));
    }
    done(err,res);
  });

};
