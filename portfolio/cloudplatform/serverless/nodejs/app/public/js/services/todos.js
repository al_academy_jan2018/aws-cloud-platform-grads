angular.module('todoService', [])

// super simple service
// each function returns a promise object
.factory('Todos', ['$http',function($http) {
  return {
    get : function() {
      return $http.get('$APIUrl' + '/todo');
    },
    create : function(todoData) {
      return $http.post('$APIUrl' + '/todo', todoData);
    },
    delete : function(id) {
      return $http.delete('$APIUrl' + '/todo/' + id);
    }
  }
}]);
