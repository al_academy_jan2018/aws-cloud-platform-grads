# This is a template to demonstrate the Automation Logic Cloudplatform using a Serverless Deployment Mechanism.
# This template is significantly different from the other Cloudplatform templates and has required a significant rewrite.
# There are a few missing features which will be added in due course.
#
# TODO:
# Static Web App S3 Bucket logging.
# Metrics and Dashboard for API Lambda logs.
# Metrics and Dashboard for API Gateway
# Elastic Search integration ?
# Parameter store for secrets
# Log Migration Process
# Notifications
# Parameterize CloudFront
# Pipeline with GIT HUB
# Auto Cleanup & Cleanup.SH for US-EAST-1 ACM Cert

AWSTemplateFormatVersion: '2010-09-09'
Description: >
  Automation Logic Cloud Platform (Serverless)
# Mappings:
# Outputs:
Parameters:
  ResourcePrefix:
    AllowedPattern: '[a-z][a-z0-9\-]*'
    ConstraintDescription: must begin with a letter and contain only lowercase alphanumeric characters.
    Default: presentation
    Description: An easily identifiable prefix for the stack's resources. Enter the Stack Name of your choice.
    MaxLength: 16
    MinLength: 1
    Type: String
  Cleanup:
    AllowedValues:
      - Enable
      - Disable
    Default: Enable
    Description: Auto Cleanup this products resources when the stack is deleted to simplify maintenance during testing. Do Not Enable on Production
    Type: String
  AdvancedLogging:
    Description: Enable/Disable Advanced logging
    AllowedValues:
      - Enable
      - Disable
    Default: Disable
    Type: String
  DBName:
    AllowedPattern: '[a-zA-Z][a-zA-Z0-9]*'
    ConstraintDescription: must begin with a letter and contain only alphanumeric characters.
    Default: todo
    Description: The database name
    MaxLength: 64
    MinLength: 1
    Type: String
  DBEngine:
    AllowedValues: ["DynamoDB"]
    Default: DynamoDB
    Description: Which DB Engine to use
    Type: String
  DomainName:
    Description: The domain name of your environment. Please note that the domain must be managed by Route53/AWS otherwise the stack creation will fail.
    AllowedPattern: '^(?!.{256})([a-zA-Z0-9][a-zA-Z0-9-]{0,61}[a-zA-Z0-9]\.){1,}([a-zA-Z]{2,3})$'
    Default: al-labs.co.uk
    Type: String
  OperationsEmail:
    Description: Email address to notify if there are any autoscaling operations or application errors
    Type: String
    AllowedPattern: (^$|([a-zA-Z0-9_\-\.+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?))
    ConstraintDescription: Must be a valid email address.
    Default: ''
  EmailApprovals:
    Description: Email used to send approval notifications
    Type: String
    AllowedPattern: (^$|([a-zA-Z0-9_\-\.+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?))
    ConstraintDescription: Must be a valid email address.
    Default: ''
  SlackApprovalsWebhook:
    Description: |
      Slack channel incoming webhook to send approval notifications, check
      https://api.slack.com/incoming-webhooks
      If no URL is set, slack notifications will be disabled/not provisioned
    Default: ''
    NoEcho: true
    Type: String

Metadata:
  AWS::CloudFormation::Interface:
    ParameterGroups:
      - Label:
          default: 'Resource Prefix'
        Parameters:
          - ResourcePrefix
      - Label:
          default: 'Domain Name:'
        Parameters:
          - DomainName
      - Label:
          default: 'Database Configuration:'
        Parameters:
          - DBEngine
          - DBName
      - Label:
          default: 'Notification:'
        Parameters:
          - OperationsEmail
          - EmailApprovals
          - SlackApprovalsWebhook
      - Label:
          default: 'Logging:'
        Parameters:
          - AdvancedLogging
      - Label:
          default: 'Debug Mode'
        Parameters:
          - Cleanup
    ParameterLabels:
      AdvancedLogging:
        default: Enable Advanced Logging Features
      ResourcePrefix:
        default: Resource Prefix Name / Stack Name
      DBName:
        default: Database Name
      DomainName:
        default: Domain Name
      EmailApprovals:
        default: Approval Email Address
      OperationsEmail:
        default: Notification Email Address
      SlackApprovalsWebhook:
        default: Slack Channel Webhook
      Cleanup:
        default: Auto Cleanup Stack

Conditions:
  cEnableCleanup: !Equals [!Ref Cleanup, 'Enable']
  cEnableAdvancedLogging: !Equals [!Ref AdvancedLogging, 'Enable']
  cEnableApprovalEmails: !Not [!Equals [!Ref EmailApprovals, '']]
  cEnableSlackApprovals: !Not [!Equals [!Ref SlackApprovalsWebhook, '']]
  cEnableEmailNotications: !Not [!Equals [!Ref OperationsEmail, '']]
  cEnableSlackAndLambdaLogging: !And [!Condition cEnableSlackApprovals, !Condition cEnableAdvancedLogging]
  cEnableEmailNoticationsAndLambdaLogging: !And [!Condition cEnableEmailNotications, !Condition cEnableAdvancedLogging]
# Resources documentation
# http://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-template-resource-type-ref.html
Resources:
  ####################################################################################
  # Cleanup
  ####################################################################################
  AutoCleanup:
    Type: Custom::AutoCleanup
    Condition: cEnableCleanup
    Properties:
      ServiceToken: !ImportValue 'CleanupLambdaArn'
      BucketNames:
        - !Ref 'ArtifactBucket'
        - !Ref 'ApplicationBucket'
      LogGroupNames:
        - !Ref 'CodeBuildWebAppLogGroup'
        - !Ref 'CodeBuildLambdaLogGroup'
        - !If
          - cEnableAdvancedLogging
          - !Ref 'EncryptLambdaLogsGroup'
          - !Ref 'AWS::NoValue'
        - !If
          - cEnableAdvancedLogging
          - !Ref 'ACMLambdaLogsGroup'
          - !Ref 'AWS::NoValue'
      StackNames:
        - !Sub '${AWS::StackName}-serverless-lambda'
  ####################################################################################
  # Start of Set Parameter Function
  # In the Serverless Cloud Platform Demo. We don't use Parameter Store for Secrets Management.
  # Instead Secrets Management is handled automatically with IAM.
  # However the Parameter Store Secrets Management Module has been left available for future Integration
  ####################################################################################
  MyKey:
    Type: AWS::KMS::Key
    Properties:
      Description: Master key used by Parameter Store
      KeyPolicy:
        Version: '2012-10-17'
        Statement:
          - Sid: Allow administration of the key
            Effect: Allow
            Principal:
              AWS: !Sub 'arn:aws:iam::${AWS::AccountId}:root'
            Action: '*'
            Resource: '*'
          - Effect: Allow
            Action:
              - kms:Encrypt
            Principal:
              AWS: !GetAtt 'EncryptLambdaRole.Arn'
            Resource: '*'
  # TODO: DECRYPT PERMISSIONS
  MyKeyAlias:
    Type: AWS::KMS::Alias
    Properties:
      AliasName: !Sub 'alias/${ResourcePrefix}-masterkey'
      TargetKeyId: !Ref MyKey

  # Create Log Group to setup the Retention time of Lambda Logs
  EncryptLambdaLogsGroup:
    Type: AWS::Logs::LogGroup
    DeletionPolicy: Retain
    Condition: cEnableAdvancedLogging
    Properties:
      LogGroupName: !Sub '/aws/lambda/${ResourcePrefix}-set-param-store'
      RetentionInDays: 1
  EncryptLambdaRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: !Sub '${ResourcePrefix}-Lambda-SetParamStore'
      AssumeRolePolicyDocument:
        Version: '2012-10-17'
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - lambda.amazonaws.com
            Action:
              - sts:AssumeRole
      Policies:
        - PolicyName: root
          PolicyDocument:
            Statement:
              # Create Log streams and write on it
              - Effect: Allow
                Action:
                  - logs:CreateLogStream
                  - logs:PutLogEvents
                Resource:
                  - !Sub 'arn:aws:logs:${AWS::Region}:${AWS::AccountId}:log-group:/aws/lambda/${ResourcePrefix}-set-param-store'
                  - !Sub 'arn:aws:logs:${AWS::Region}:${AWS::AccountId}:log-group:/aws/lambda/${ResourcePrefix}-set-param-store:*'
              # Allow Lambda Function to set values into Parameters Store in a specific name space
              - Effect: Allow
                Action:
                  - ssm:PutParameter
                  - ssm:DeleteParameter
                Resource:
                  - !Sub 'arn:aws:ssm:${AWS::Region}:${AWS::AccountId}:parameter/${AWS::StackName}/db/*'
                  # Used on AWS console to test the function ^.^
                  - !Sub 'arn:aws:ssm:${AWS::Region}:${AWS::AccountId}:parameter/${AWS::StackName}/fn.test'
  EncryptLambda:
    Type: AWS::Lambda::Function
    DependsOn: MyKeyAlias
    Properties:
      FunctionName: !Sub '${ResourcePrefix}-set-param-store'
      Handler: index.lambda_handler
      Role: !GetAtt 'EncryptLambdaRole.Arn'
      Code:
        # Lambda to Store Values in SSM Parameter Store.
        # Ref: lambda/SetEC2Parameter.js
        ZipFile: |
          var AWS = require('aws-sdk');
          var response = require('cfn-response');
          exports.lambda_handler = function (event, context) {
            try {
              var ssm = new AWS.SSM();
              if(event.RequestType == 'Delete') {
                var params = {
                  Name: event.ResourceProperties.Name
                }
                ssm.deleteParameter(params, function(err, data) {
                  console.log('Delete Called');
                  if (err){
                    console.log('Error: ',err, err.stack); // an error occurred
                  }
                  else{
                    console.log('Successfully Deleted Parameter: ',data);           // successful response
                  }
                });
                response.send(event, context, response.SUCCESS);
              }
              else {
                var params = {
                  Name: event.ResourceProperties.Name,
                  Type: event.ResourceProperties.Type,
                  Description: event.ResourceProperties.Description,
                  Value: event.ResourceProperties.Value,
                  KeyId: event.ResourceProperties.KeyId,
                  Overwrite: true
                };
                ssm.putParameter(params, function(err, data) {
                    if (err){
                      console.log('Error: ',err, err.stack); // an error occurred
                      response.send(event, context, response.FAILED);
                    }
                    else{
                      console.log('Successfully Put Parameter: ',data);           // successful response
                      response.send(event, context, response.SUCCESS);
                    }
                });
              }
            }
            catch (err) {
              console.log('General Error.');
              console.log(err);
              response.send(event, context, response.FAILED);
            }
          };
      Runtime: nodejs6.10
      Timeout: 25
  ####################################################################################
  # Start of Database
  ####################################################################################
  DBInstance:
    Type: "AWS::DynamoDB::Table"
    Properties:
      TableName: !Ref DBName
      AttributeDefinitions:
        - AttributeName: "_id"
          AttributeType: "S"
      KeySchema:
        - AttributeName: "_id"
          KeyType: "HASH"
      ProvisionedThroughput:
        ReadCapacityUnits: 1
        WriteCapacityUnits: 1
  WriteCapacityScalableTarget:
    Type: "AWS::ApplicationAutoScaling::ScalableTarget"
    Properties:
      MaxCapacity: 15
      MinCapacity: 1
      ResourceId: !Join
        - /
        - - table
          - !Ref DBInstance
      RoleARN: !GetAtt ScalingRole.Arn
      ScalableDimension: dynamodb:table:WriteCapacityUnits
      ServiceNamespace: dynamodb
  ScalingRole:
    Type: "AWS::IAM::Role"
    Properties:
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: "Allow"
            Principal:
              Service:
                - application-autoscaling.amazonaws.com
            Action:
              - "sts:AssumeRole"
      Path: "/"
      Policies:
        - PolicyName: "root"
          PolicyDocument:
            Version: "2012-10-17"
            Statement:
              - Effect: "Allow"
                Action:
                  - "dynamodb:DescribeTable"
                  - "dynamodb:UpdateTable"
                  - "cloudwatch:PutMetricAlarm"
                  - "cloudwatch:DescribeAlarms"
                  - "cloudwatch:GetMetricStatistics"
                  - "cloudwatch:SetAlarmState"
                  - "cloudwatch:DeleteAlarms"
                Resource: "*"
  WriteScalingPolicy:
    Type: "AWS::ApplicationAutoScaling::ScalingPolicy"
    Properties:
      PolicyName: WriteAutoScalingPolicy
      PolicyType: TargetTrackingScaling
      ScalingTargetId: !Ref WriteCapacityScalableTarget
      TargetTrackingScalingPolicyConfiguration:
        TargetValue: 50.0
        ScaleInCooldown: 60
        ScaleOutCooldown: 60
        PredefinedMetricSpecification:
          PredefinedMetricType: DynamoDBWriteCapacityUtilization
  StoreDBName:
    Type: AWS::SSM::Parameter
    DeletionPolicy: Delete
    Properties:
      Name: !Sub '/${AWS::StackName}/db/name'
      Value: !Ref DBName
      Description: DataBase Name
      Type: 'String'
  ####################################################################################
  # Appication Front-End
  ####################################################################################
  ApplicationBucket:
    Type: AWS::S3::Bucket
    DeletionPolicy: Retain
    Properties:
      BucketName: !Sub '${ResourcePrefix}.${DomainName}'
      AccessControl: 'PublicRead'
      WebsiteConfiguration:
        IndexDocument: 'index.html'
  ApplicationBucketPolicy:
    Type: "AWS::S3::BucketPolicy"
    Properties:
      Bucket:
        Ref: ApplicationBucket
      PolicyDocument:
        Statement:
          - Action:
              - "s3:GetObject"
            Effect: "Allow"
            Resource: !Sub 'arn:aws:s3:::${ApplicationBucket}/*'
            Principal: "*"
  # Since Cloudfront only accepts ACM certificates in the US-east-1 region, We cannot use cloudformation "AWS::CertificateManager::Certificate"
  # Instead a Dynamic Lambda function is used to generate the ACM certificate with the help of a Custom Resource.
  ACMLambdaLogsGroup:
    Type: AWS::Logs::LogGroup
    DeletionPolicy: Retain
    Condition: cEnableAdvancedLogging
    Properties:
      LogGroupName: !Sub '/aws/lambda/${ResourcePrefix}-ACM-Lambda'
      RetentionInDays: 1
  ACMLambdaRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: !Sub '${ResourcePrefix}-ACM-Lambda-Role'
      AssumeRolePolicyDocument:
        Version: '2012-10-17'
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - lambda.amazonaws.com
            Action:
              - sts:AssumeRole
      Policies:
        - PolicyName: root
          PolicyDocument:
            Statement:
              # Create Log streams and write on it
              - Effect: Allow
                Action:
                  - logs:CreateLogStream
                  - logs:PutLogEvents
                Resource:
                  - !Sub 'arn:aws:logs:${AWS::Region}:${AWS::AccountId}:log-group:/aws/lambda/${ResourcePrefix}-ACM-Lambda'
                  - !Sub 'arn:aws:logs:${AWS::Region}:${AWS::AccountId}:log-group:/aws/lambda/${ResourcePrefix}-ACM-Lambda:*'
              - Effect: Allow
                Action:
                  - acm:RequestCertificate
                Resource: '*'
  ACMLambda:
    Type: AWS::Lambda::Function
    Properties:
      FunctionName: !Sub '${ResourcePrefix}-ACM-Lambda'
      Handler: index.handler
      Role: !GetAtt 'ACMLambdaRole.Arn'
      Code:
        # Lambda to Request an ACM SSL certificate in the us-east-1 region.
        # Ref: lambda/ACM-Request-Cert.js
        ZipFile: |
          var AWS = require('aws-sdk');
          var response = require('cfn-response');
          AWS.config.update({region: 'us-east-1'});
          exports.handler = function (event, context) {
            try {
              var acm = new AWS.ACM();
              if(event.RequestType == 'Delete') {
                console.log('Delete Called');
                response.send(event, context, response.SUCCESS);
              }
              else{
                var params = {
                  DomainName: event.ResourceProperties.DomainName,
                  DomainValidationOptions: [
                    {
                      DomainName: event.ResourceProperties.DomainName,
                      ValidationDomain: event.ResourceProperties.ValidationDomain
                    },
                  ],
                  SubjectAlternativeNames: event.ResourceProperties.SubjectAlternativeNames
                };
                acm.requestCertificate(params, function(err, data) {
                  if (err){
                    console.log(err, err.stack); // an error occurred
                    response.send(event, context, response.FAILED);
                  }
                  else{
                    console.log(data);           // successful response
                    response.send(event, context, response.SUCCESS, data);
                  }
                });
              }
            }
            catch (err) {
              console.log('General Error.');
              console.log(err);
              response.send(event, context, response.FAILED);
            }
          };
      Runtime: nodejs6.10
      Timeout: 25
  CallACMLambda:
    Type: Custom::ACMRequest
    DependsOn:
      - MyKeyAlias
    Properties:
      ServiceToken: !GetAtt 'ACMLambda.Arn'
      DomainName: !Sub '${ResourcePrefix}.${DomainName}'
      ValidationDomain: !Sub '${DomainName}'
  ####################################################################################
  # Start of CI / CD
  ####################################################################################
  CodeCommitRepo:
    Type: AWS::CodeCommit::Repository
    # Intentional depends on condition to create an artificial delay. Explained in detail above.
    DeletionPolicy: Delete
    Properties:
      RepositoryDescription: Serverless code and the Cloud Formation template to deploy it
      RepositoryName: !Sub '${ResourcePrefix}-Repo'
  ArtifactBucket:
    Type: AWS::S3::Bucket
    DeletionPolicy: Retain
    Properties:
      BucketName: !Sub '${ResourcePrefix}-artifacts'
  CodeBuildRole:
    Type: AWS::IAM::Role
    DependsOn: ArtifactBucket
    Properties:
      RoleName: !Sub ${ResourcePrefix}-CodeBuildRole
      AssumeRolePolicyDocument:
        Version: '2012-10-17'
        Statement:
          - Effect: Allow
            Principal:
              Service: [codebuild.amazonaws.com]
            Action: ['sts:AssumeRole']
      Path: /
      Policies:
        - PolicyName: !Sub '${ResourcePrefix}-CodeBuildPolicy'
          PolicyDocument:
            Statement:
              - Effect: Allow
                Action:
                  - s3:PutObject
                  - s3:GetBucketPolicy
                  - s3:GetObject
                  - s3:ListBucket
                Resource:
                  - !Sub 'arn:aws:s3:::${ArtifactBucket}/*'
                  - !Sub 'arn:aws:s3:::${ArtifactBucket}'
                  - !Sub 'arn:aws:s3:::${ApplicationBucket}/*'
                  - !Sub 'arn:aws:s3:::${ApplicationBucket}'
              - Effect: Allow
                Action:
                  - apigateway:GET
                Resource:
                  - !Sub 'arn:aws:apigateway:${AWS::Region}::/restapis'
              - Effect: Allow
                Action:
                  - cloudfront:CreateInvalidation
                  - cloudfront:ListDistributions
                Resource: "*"
              - Effect: Allow
                Action:
                  - logs:CreateLogStream
                  - logs:PutLogEvents
                Resource:
                  - !Sub 'arn:aws:logs:${AWS::Region}:${AWS::AccountId}:log-group:/aws/codebuild/${ResourcePrefix}-CodeBuildLambda:log-stream'
                  - !Sub 'arn:aws:logs:${AWS::Region}:${AWS::AccountId}:log-group:/aws/codebuild/${ResourcePrefix}-CodeBuildLambda:log-stream:*'
                  - !Sub 'arn:aws:logs:${AWS::Region}:${AWS::AccountId}:log-group:/aws/codebuild/${ResourcePrefix}-CodeBuildWebApp:log-stream'
                  - !Sub 'arn:aws:logs:${AWS::Region}:${AWS::AccountId}:log-group:/aws/codebuild/${ResourcePrefix}-CodeBuildWebApp:log-stream:*'
                  - !Sub 'arn:aws:logs:${AWS::Region}:${AWS::AccountId}:log-group:/aws/codebuild/${ResourcePrefix}-products:log-stream'
                  - !Sub 'arn:aws:logs:${AWS::Region}:${AWS::AccountId}:log-group:/aws/codebuild/${ResourcePrefix}-products:log-stream:*'
  CodeBuildWebAppLogGroup:
    Type: AWS::Logs::LogGroup
    DeletionPolicy: Delete
    Properties:
      LogGroupName: !Sub '/aws/codebuild/${ResourcePrefix}-CodeBuildWebApp'
      RetentionInDays: 30
  CodeBuildWebApp:
    Type: AWS::CodeBuild::Project
    DependsOn: [CodeCommitRepo, CodeBuildRole, CodeBuildWebAppLogGroup]
    Properties:
      Name: !Sub '${ResourcePrefix}-CodeBuildWebApp'
      Description: !Sub '${ResourcePrefix}-CodeBuildWebApp'
      ServiceRole: !GetAtt CodeBuildRole.Arn
      Artifacts:
        Type: CODEPIPELINE
      Environment:
        Type: LINUX_CONTAINER
        ComputeType: BUILD_GENERAL1_SMALL
        Image: aws/codebuild/nodejs:7.0.0
        EnvironmentVariables:
          - Name: WEB_BUCKET
            Value: !Ref ApplicationBucket
          - Name: Resource_Prefix
            Value: !Sub '${ResourcePrefix}'
          - Name: REGION
            Value: !Sub '${AWS::Region}'
          - Name: WEB_BUCKET_URL
            Value: !Sub '${ApplicationBucket.DomainName}'
      Source:
        Type: CODEPIPELINE
        BuildSpec: |
          version: 0.2
          phases:
            install:
              commands:
                - cd portfolio/cloudplatform/serverless/nodejs/app/public/js/services
            pre_build:
                commands:
                - API_ID="$(aws apigateway get-rest-apis --region "${REGION}" --query "items[?name=='"${Resource_Prefix}"-APIGateway'].id" --output text)"
                - DNS_NAME="https://$API_ID.execute-api.${REGION}.amazonaws.com/v1"
                - sed -i 's@$APIUrl@'"$DNS_NAME"'@g' todos.js
            build:
              commands:
                - cd ../..
                - aws s3 cp . s3://${WEB_BUCKET} --recursive
            post_build:
              commands:
                # The AWS CLI tools installed on the code build server are an older version where cloudfront is in preview mode.
                # Determine the Distribution ID of the cloudfront distribution and create an invalidation to ensure that the cache is invalidated.
                - aws configure set preview.cloudfront true
                - DIST_ID="$(aws cloudfront list-distributions --query "DistributionList.Items[?Origins.Items[?DomainName=='"${WEB_BUCKET_URL}"']].Id" --output text)"
                - aws cloudfront create-invalidation --distribution-id $DIST_ID --paths "/*"
      TimeoutInMinutes: 10
      Tags:
        - Key: Name
          Value: !Sub '${ResourcePrefix}'
  CodeBuildLambdaLogGroup:
    Type: AWS::Logs::LogGroup
    DeletionPolicy: Delete
    Properties:
      LogGroupName: !Sub '/aws/codebuild/${ResourcePrefix}-CodeBuildLambda'
      RetentionInDays: 30
  CodeBuildLambda:
    Type: AWS::CodeBuild::Project
    DependsOn: [CodeCommitRepo, CodeBuildRole, CodeBuildLambdaLogGroup]
    Properties:
      Name: !Sub '${ResourcePrefix}-CodeBuildLambda'
      Description: !Sub '${ResourcePrefix}-CodeBuildLambda'
      ServiceRole: !GetAtt CodeBuildRole.Arn
      Artifacts:
        Type: CODEPIPELINE
      Environment:
        Type: LINUX_CONTAINER
        ComputeType: BUILD_GENERAL1_SMALL
        Image: aws/codebuild/nodejs:7.0.0
        EnvironmentVariables:
          - Name: S3_BUCKET
            Value: !Ref ArtifactBucket
          - Name: AWS_REGION
            Value: !Ref AWS::Region
          # We are forming the ARN since the lambda does not exist at this point.
          # If the Lambda Name in the Serverless.yaml template is changed, the arn here needs to be changed as well.
          - Name: GetAPILambda_Arn
            Value: !Sub 'arn:aws:lambda:${AWS::Region}:109964479621:function:${ResourcePrefix}-GETAPILambda'
          - Name: PostAPILambda_Arn
            Value: !Sub 'arn:aws:lambda:${AWS::Region}:109964479621:function:${ResourcePrefix}-POSTAPILambda'
          - Name: DeleteAPILambda_Arn
            Value: !Sub 'arn:aws:lambda:${AWS::Region}:109964479621:function:${ResourcePrefix}-DELETEAPILambda'
      Source:
        Type: CODEPIPELINE
        BuildSpec: |
          version: 0.2
          phases:
            install:
              commands:
                - cd portfolio/cloudplatform/serverless/nodejs/app/lambda/
                - npm install
            pre_build:
                commands:
                - sed -i 's/$AWSRegion/'"${AWS_REGION}"'/g' swagger.yaml
                - sed -i 's/$GetLambdaArn/'"${GetAPILambda_Arn}"'/g' swagger.yaml
                - sed -i 's/$PostLambdaArn/'"${PostAPILambda_Arn}"'/g' swagger.yaml
                - sed -i 's/$DeleteLambdaArn/'"${DeleteAPILambda_Arn}"'/g' swagger.yaml
            build:
              commands:
                - aws cloudformation package --template-file serverless.yaml --output-template-file lambdatemplate.yaml --s3-bucket "${S3_BUCKET}" --s3-prefix serverless-template/codebuild
          artifacts:
            files: portfolio/cloudplatform/serverless/nodejs/app/lambda/lambdatemplate.yaml
            discard-paths: yes
      TimeoutInMinutes: 10
      Tags:
        - Key: Name
          Value: !Sub '${ResourcePrefix}'
  CFDeployerRole:
    Type: AWS::IAM::Role
    DependsOn: ArtifactBucket
    Properties:
      RoleName: !Sub '${ResourcePrefix}-${AWS::Region}-cfdeployer-role'
      AssumeRolePolicyDocument:
        Version: '2012-10-17'
        Statement:
          - Effect: Allow
            Principal:
              Service: [cloudformation.amazonaws.com]
            Action: ['sts:AssumeRole']
      Path: /
      Policies:
        - PolicyName: !Sub '${ResourcePrefix}-${AWS::Region}-cfdeployer-policy'
          PolicyDocument:
            Version: '2012-10-17'
            Statement:
              - Effect: Allow
                Action:
                  - lambda:AddPermission
                  - lambda:CreateFunction
                  - lambda:DeleteFunction
                  - lambda:InvokeFunction
                  - lambda:RemovePermission
                  - lambda:UpdateFunctionCode
                  - lambda:GetFunctionConfiguration
                  - lambda:GetFunction
                  - lambda:UpdateFunctionConfiguration
                  - iam:CreateRole
                  - iam:CreatePolicy
                  - iam:GetRole
                  - iam:DeleteRole
                  - iam:PutRolePolicy
                  - iam:PassRole
                  - iam:DeleteRolePolicy
                  - cloudformation:*
                  - logs:*
                  - apigateway:*
                  - cloudfront:*
                  - route53:*
                Resource: "*"
              - Effect: Allow
                Action:
                  - s3:PutObject
                  - s3:GetBucketPolicy
                  - s3:GetObject
                  - s3:ListBucket
                Resource:
                  - !Sub 'arn:aws:s3:::${ArtifactBucket}/*'
                  - !Sub 'arn:aws:s3:::${ArtifactBucket}'
  CodePipeLineRole:
    Type: AWS::IAM::Role
    DependsOn: ArtifactBucket
    Properties:
      RoleName: !Sub '${ResourcePrefix}-${AWS::Region}-role'
      AssumeRolePolicyDocument:
        Version: '2012-10-17'
        Statement:
          - Effect: Allow
            Principal:
              Service: [codepipeline.amazonaws.com]
            Action: ['sts:AssumeRole']
      Path: /
      Policies:
        - PolicyDocument:
            Version: '2012-10-17'
            Statement:
              - Effect: Allow
                Action:
                  - codepipeline:*
                  - iam:ListRoles
                  - cloudformation:*
                  - codecommit:List*
                  - codecommit:Get*
                  - codecommit:GitPull
                  - codecommit:UploadArchive
                  - codecommit:CancelUploadArchive
                  - codebuild:BatchGetBuilds
                  - codebuild:StartBuild
                  - iam:PassRole
                  - s3:ListAllMyBuckets
                  - s3:GetBucketLocation
                  - lambda:InvokeFunction
                  - lambda:ListFunctions
                  - lambda:GetFunctionConfiguration
                Resource:
                  - "*"
              - Effect: Allow
                Action:
                  - s3:PutObject
                  - s3:GetBucketPolicy
                  - s3:GetObject
                  - s3:ListBucket
                Resource:
                  - !Sub 'arn:aws:s3:::${ArtifactBucket}/*'
                  - !Sub 'arn:aws:s3:::${ArtifactBucket}'
          PolicyName: !Sub '${ResourcePrefix}-${AWS::Region}-policy'
  Pipeline:
    Type: AWS::CodePipeline::Pipeline
    DependsOn: [CodePipeLineRole, CodeBuildLambda, CFDeployerRole]
    Properties:
      RoleArn: !GetAtt CodePipeLineRole.Arn
      Name: !Ref AWS::StackName
      Stages:
        - Name: source-code-checkout
          Actions:
            - Name: App
              ActionTypeId:
                Category: Source
                Owner: AWS
                Version: 1
                Provider: CodeCommit
              Configuration:
                RepositoryName: !GetAtt CodeCommitRepo.Name
                BranchName: master
              OutputArtifacts:
                - Name: SCCheckoutArtifact
              RunOrder: 1
        - Name: Build-And-Deploy-API
          Actions:
            - Name: build-lambda-function
              ActionTypeId:
                Category: Build
                Owner: AWS
                Version: 1
                Provider: CodeBuild
              Configuration:
                ProjectName: !Ref CodeBuildLambda
              RunOrder: 1
              InputArtifacts:
                - Name: SCCheckoutArtifact
              OutputArtifacts:
                - Name: BuildLambdaOutput
            - Name: deploy-lambda-function
              ActionTypeId:
                Category: Deploy
                Owner: AWS
                Version: 1
                Provider: CloudFormation
              Configuration:
                ChangeSetName: Deploy
                ActionMode: CREATE_UPDATE
                StackName: !Sub '${AWS::StackName}-serverless-lambda'
                Capabilities: CAPABILITY_NAMED_IAM
                TemplatePath: BuildLambdaOutput::lambdatemplate.yaml
                RoleArn: !GetAtt CFDeployerRole.Arn
                ParameterOverrides: !Sub |
                  {
                    "ParamsPrefix": "${ResourcePrefix}",
                    "DynamoTableName": "${DBName}",
                    "CertificateArn": "${CallACMLambda.CertificateArn}",
                    "OriginBucket": "${ApplicationBucket.DomainName}",
                    "DomainName": "${DomainName}"
                  }
              InputArtifacts:
                - Name: BuildLambdaOutput
              RunOrder: 2
        - Name: Build-And-Deploy-WebApp
          Actions:
            - Name: build-web-app
              ActionTypeId:
                Category: Build
                Owner: AWS
                Version: 1
                Provider: CodeBuild
              Configuration:
                ProjectName: !Ref CodeBuildWebApp
              RunOrder: 1
              InputArtifacts:
                - Name: SCCheckoutArtifact
      ArtifactStore:
        Type: S3
        Location: !Ref ArtifactBucket
Outputs:
  ApplicationURL:
    Value: !Sub 'https://${ResourcePrefix}.${DomainName}'
    Description: The URL of the Web Application.
  RepoURL:
    Description: Git Repository
    Value: !GetAtt 'CodeCommitRepo.CloneUrlSsh'
  PipelineUrl:
    Value: !Sub 'https://console.aws.amazon.com/codepipeline/home?region=${AWS::Region}#/view/${Pipeline}'
  CertificateArn:
    Value: !Sub '${CallACMLambda.CertificateArn}'
