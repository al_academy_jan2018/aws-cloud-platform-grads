#!/usr/bin/env bash

set -xe

cd /app
chmod +x scripts/*.sh

# Update database and connexion parameters
./scripts/entrypoint.sh ./scripts/sql_schema.sh
