#!/usr/bin/env bash

set -xe

echo "[WebApp] Starting App"
nohup /app/scripts/entrypoint.sh python /app/sample.py 1>/tmp/server.log 2>/tmp/server.err &
echo "[WebApp] App started"
