from flask import Flask, render_template, request, json
from flaskext.mysql import MySQL
import os

mysql = MySQL()
application = Flask(__name__)

# MySQL Configuration
application.config['MYSQL_DATABASE_USER'] = os.environ["DB_USERNAME"]
application.config['MYSQL_DATABASE_PASSWORD'] = os.environ["DB_PASSWORD"]
application.config['MYSQL_DATABASE_DB'] = os.environ["DB_NAME"]
application.config['MYSQL_DATABASE_HOST'] = os.environ["DB_CONNECTIONSTRING"]
mysql.init_app(application)

@application.route('/')
def main():
    return render_template('index.html')

# Insert "name" into MySQL database
@application.route('/insert', methods=['POST', 'GET'])
def insert():
    try:
        name = request.form['inputName']
        if name:
            conn = mysql.connect()
            cursor = conn.cursor()
            cursor.execute("INSERT INTO todo (name) VALUES (%s)", (name))
            data = cursor.fetchall()
            if len(data) is 0:
                conn.commit()
                return json.dumps({'message':'Your name is forever stored successfully!'})
            else:
                return json.dumps({'error':str(data[0])})
        else:
            return json.dumps({'html':'<span>Enter your name!</span>'})
    except Exception as e:
        return json.dumps({'error':str(e)})
    finally:
        cursor.close()
        conn.close()

# Pull "names" from database and display it
@application.route('/getData', methods=['POST', 'GET'])
def getData():
    try:
        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM todo")
        wishes = cursor.fetchall()

        wishes_dict = []
        for wish in wishes:
            wish_dict = {
            'Id': wish[0],
            'Name': wish[1]}
            wishes_dict.append(wish_dict)

        return json.dumps(wishes_dict)

    except Exception as e:
        return json.dumps({'error':str(e)})
    finally:
        cursor.close()
        conn.close()

if __name__ == "__main__":
    application.run(host='0.0.0.0', port=os.environ["PORT"])
