#!/usr/bin/env bash

set -xeu
cd /app

# Give execution permissions to the scripts
chmod u+x ./scripts/*.sh

# Copy files over the apache html folder
cp -rv ./public/* /var/www/html/

# Amend the apache config to use main.php as the default index file
sed -i 's/DirectoryIndex/DirectoryIndex main.php/g' /etc/apache2/mods-enabled/dir.conf

# Allowing for customized port
port=$(./scripts/entrypoint.sh env | grep '^PORT=' | cut -d= -f2)
sed -i "s/^Listen 80$/Listen ${port}/g" /etc/apache2/ports.conf

# Update database and connection settings
./scripts/entrypoint.sh /app/scripts/sql_schema.sh

# Reload apache configuration (for containers config the service will be started afterwards)
service apache2 reload || true
