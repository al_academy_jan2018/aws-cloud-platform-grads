- [Description of files](#description-of-files)
  - [portfolio/](#portfolio)
    - [central-logging/](#central-logging)
    - [cloudplatform/ubuntu/node-sql/](#cloudplatformubuntunode-sql)
  - [servicecatalogue/lambda](#servicecataloguelambda)

# Description of files

```
.
├── LICENSE.txt
├── README.md
├── mastertemplate.yaml
├── portfolio
│   ├── central-logging
│   │   └── central-logging-elasticsearch.yaml
│   ├── cloudplatform
│   │   └── ubuntu
│   │       └── node-sql
│   │           ├── app
│   │           │   ├── appspec.yml
│   │           │   └── containerapp.template.yaml
│   │           │   └── Dockerfile
│   │           └── cloudplatform.yaml
│   └── mappings.yaml
└── servicecatalogue
    └── lambda
        ├── lambda-cloudformation.yaml
        ├── requirements.txt
        └── sync-catalog.py
```

 * mastertemplate.yaml -> Template for _CodePipeline_ to manage a _Service Catalog_. The pipeline consists of a _CodeCommit_ repository and the steps to manage the portfolio and its products.

## portfolio/
 * mappings.yaml -> Instructions for the _CodePipeline_ defining the _Service Catalog_ portfolio and a list of products that are a part of it.

### central-logging/
 * central-logging-elasticsearch.yaml -> A central-logging Elasticsearch cluster that exports an endpoint so infrastructure and application stacks can stream data to it.

### cloudplatform/ubuntu/node-sql/
The other ALCP product stacks have near identical file structure.

 * cloudplatform.yaml -> Main _CloudFormation_ template for infrastructure deployment.
 * app/containerapp.template.yaml -> Used by the _CodePipeline_ defined in cloudplatform.yaml to deploy an alternative container backend for the application.
 * app/Dockerfile -> The docker file with instructions used to create the docker containers when the Use Containers option is selected.
 * app/appspec.yml -> The file with deployment instructions which is used by AWS Code Deploy to deploy changes as part of the CI/CD pipeline.

## servicecatalogue/lambda

 * lambda-cloudformation.yaml -> **CloudFormation** template to deploy a stack with the lambda code from this directory and its supporting resources.
 * requirements.txt -> Python dependencies to be packaged with lambda code.
 * sync-catalog.py -> Code of the lambda used to manage the _Service Catalog_ portfolio.

