- [Parameters Conditions Explained](#parameters-conditions-explained)
  - [__mastertemplate.yaml (service stack)__](#mastertemplateyaml-service-stack)
      - [Debug Mode](#debug-mode)
    - [__portfolio/cloudplatform/ubuntu/node-sql/cloudplatform.yaml (product stack)__](#portfoliocloudplatformubuntunode-sqlcloudplatformyaml-product-stack)
      - [Resource Prefix](#resource-prefix)
      - [Domain Name](#domain-name)
      - [Database Configuration](#database-configuration)
      - [Application Settings](#application-settings)
      - [Auto-Scaling Web Server Group's Instance Configuration](#auto-scaling-web-server-groups-instance-configuration)
      - [Security](#security)
      - [Notification](#notification)
      - [Logging](#logging)
      - [Debug Mode](#debug-mode)
    - [__portfolio/central-logging/central-logging-elasticsearch.yaml (product stack)__](#portfoliocentral-loggingcentral-logging-elasticsearchyaml-product-stack)
      - [Resource Prefix](#resource-prefix)
      - [CloudTrail](#cloudtrail)
      - [Elastic Search](#elastic-search)
      - [Debug Mode](#debug-mode)

# Parameters Conditions Explained

## __mastertemplate.yaml (service stack)__
### Debug Mode
* Cleanup -> This option when enabled with create a cleanup lambda function and an cloudformation export of its ARN (in order for product stacks to find it). This lambda gets triggered when the product stack is deleted and takes care of deleting any retained resources from the product stack. This functionality is only to be used during testing. **NEVER ENABLE FOR PRODUCTION PLATFORMS**.


## __portfolio/cloudplatform/ubuntu/node-sql/cloudplatform.yaml (product stack)__
The other ALCP product stacks have near identical options.
### Resource Prefix
 * Resource Prefix -> A string which is used as the prefix for all resources created by the stack. Helps to identify stack resources. Suggested policy is:
    - use same name as the product name when provisioning through service catalogue.
    - use same name as the stack name when provisioning through cloud formation web interface.
### Domain Name
 * Use DNS -> This option when enabled will map the Elastic Load Balancer to a subdomain in al-labs.co.uk
    - The domain name can be changed in the template to use a different domain of your choice.
### Database Configuration
 * DBEngine -> Choice of back-end database to create when provisioning the stack.
    - Differs based on application type.
 * Database Name -> The Database Name to create.
 * Database UserName -> The Username to create on the database.
 * Database Password -> The password for the corresponding user account to create on the database.
 * Database Instance Class -> The Instance Type to use when creating the database.
 * Database Encryption -> Options for turning on database encryption.
 * Database Storage -> The maximum size of the database.
### Application Settings
 * Application TCP Port -> The TCP port that the application will listen on.
### Auto-Scaling Web Server Group's Instance Configuration
 * Use Spot Instances -> This option when enabled will use Spot Instances instead of On-Demand Instances. Spot Instances cost far less than On-Demand Instances and can be used in non-critical environments like development.
    - AWS will provide a 2 minute warning and then terminate the instance.It is possible to fail-over to using On-Demand Instances when this happens, but this feature has not been implemented in this platform yet.
    - **WARNING: Spot Instances can be auto terminated by AWS when the Spot Price rises above your bid price.**   
    - **UPDATE (28th July 2017):  Spot Instances work in all regions where this stack can deploy to.**  
    - All (working) regions, except Ohio, support m3.medium as the smallest acceptable spot instance type. Ohio requires m4.large.
 * Spot Instance Bid Price -> The maximum bid price for spot instance.
    - $0.02 can be the minimum set to all regions. The template has $0.05 has the default price.
 * Use Docker Containers -> Enable docker containers through ECS or Disable for EC2 VM Instances.
 * Instance Type -> Select the type of EC2 Instances to use as the web server or container host.
 * Operating System -> Choice of Operating System that will run on the web server. Some options are locked based on which demo is run.
 * Instance Count -> The Number of EC2 instances to launch during stack creation.
 * Instance Max Count -> The Maximum number of instances to auto-scale.
 * Instance Min Count -> The Minimum number of instances to auto-scale.
### Security
 * Key Pair Name -> Select your EC2 SSH key pair which is used to secure your VMs. This needs to be created before hand from the AWS EC2 web interface.
 * Use HTTPS -> This option when enabled will create a certificate using the Amazon Certificate Manager and attach it to the Elastic Load Balancer to create a secure HTTPS connection.
 * SSH Location -> The CIDR IP Address range from which to accept SSH or RDP connections. Restrict this to your organization's static IP to prevent unauthorized admin access.
### Notification
 * Notification Email Address -> A valid email address to which stack update and alarm notification emails are sent.
 * Approval Email Address -> A manager's email address to which Continuous Integration Pipeline sends an approval link which needs to be clicked to approve a build for deployment.
 * Slack Channel Webhook -> A Slack webhook URL which is used to send notifications to a Slack channel.
### Logging
 * Enable Advanced Logging Features -> When Enabled, this option will create log groups to log all activity including lambda functions on the stack. When disabled only the build server and application logs are logged. 
 * Deploy Elasticsearch -> This option when enabled will stream logs to an Elastic Search instance.
    - Central -> Requires a central logging product to be provisioned beforehand in the same region. This will create a central Elasticsearch cluster.
    - Integrated -> Each stack will create and manage its own Elasticsearch cluster.
 * Central Logging Elasticsearch Stack's Resource Prefix Name -> The resource prefix name of the central logging stack. Used to find the central ES cluster if the previous option is set to Central
 * Amazon ES cluster size -> The size of the Elastic Search cluster to create if Deploy Elasticsearch is set to Integrated.
    - Large : 6 x r3.8xlarge.elasticsearch
    - Medium : 4 x r3.2xlarge.elasticsearch
    - Small : 2 x m3.large.elasticsearch
    - Tiny : 2 x t2.medium.elasticsearch (default)
 * IP address range -> The IP range that can access an Integrated Elasticsearch cluster.
### Debug Mode
 * Auto Cleanup Stack -> When Enabled, on stack deletion, the stack will signal the cleanup lambda, on the Service Stack, which is used to delete retained resources of this stack. This feature is only meant to make testing easier and as such is NOT RECOMMENDED FOR PRODUCTION.

## __portfolio/central-logging/central-logging-elasticsearch.yaml (product stack)__
### Resource Prefix
 * Resource Prefix -> A string which is used as the prefix for all resources created by the stack. Helps to identify stack resources. Suggested policy is:
    - use same name as the product name when provisioning through service catalogue.
    - use same name as the stack name when provisioning through cloud formation web interface.
### CloudTrail
 * CloudTrailLogging -> Option to enable Cloud Trail logging in the region.
### Elastic Search
 * Amazon ES cluster size -> The size of the Elastic Search cluster to create if Deploy Elasticsearch is set to Integrated.
    - Large : 6 x r3.8xlarge.elasticsearch
    - Medium : 4 x r3.2xlarge.elasticsearch
    - Small : 2 x m3.large.elasticsearch
    - Tiny : 2 x t2.medium.elasticsearch (default)
 * IP address range -> The IP range that can access an Integrated Elasticsearch cluster.
### Debug Mode
 * Auto Cleanup Stack -> When Enabled, on stack deletion, the stack will signal the cleanup lambda, on the Service Stack, which is used to delete retained resources of this stack. This feature is only meant to make testing easier and as such is NOT RECOMMENDED FOR PRODUCTION.

